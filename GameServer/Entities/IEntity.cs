﻿using MathNet.Spatial.Euclidean;
using System;

namespace GameServer.Entities
{
    public interface IEntity
    {
        Guid Id { get; }

        Point3D Position { get; set; }
        float Rotation { get; set; }

        bool StateChanged { get; set; }
        bool PositionChanged { get; set; }

        EntityState GetState();
        EntityState GetStateUpdate();

        EntityPosition GetPosition();
    }
}
