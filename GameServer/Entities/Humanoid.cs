﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace GameServer.Entities
{
    public abstract class Humanoid<TState> : Entity<TState> where TState : HumanoidState, ICloneable, new()
    {
        private string title;
        private string name;
        private int level;
        private HumanoidPosture posture;
        private Dictionary<HumanoidAppearanceType, int> appearance;

        public Humanoid()
        {
            appearance = new Dictionary<HumanoidAppearanceType, int>();
            Appearance = new ReadOnlyDictionary<HumanoidAppearanceType, int>(appearance);
        }

        public string Title
        {
            get => title;
            set
            {
                title = value;
                stateUpdate.Title = value;
                RegisterStateUpdate();
            }
        }

        public string Name
        {
            get => name;
            set
            {
                name = value;
                stateUpdate.Name = value;
                RegisterStateUpdate();
            }
        }

        public int Level
        {
            get => level;
            set
            {
                level = value;
                stateUpdate.Level = value;
                RegisterStateUpdate();
            }
        }

        public HumanoidPosture Posture
        {
            get => posture;
            set
            {
                posture = value;
                stateUpdate.Posture = value;
                RegisterStateUpdate();
            }
        }

        public ReadOnlyDictionary<HumanoidAppearanceType, int> Appearance { get; private set; }

        public override EntityState GetState()
        {
            TState state = (TState) base.GetState();

            state.Title = Title;
            state.Name = Name;
            state.Level = Level;
            state.Posture = Posture;
            state.Appearance = appearance;

            return state;
        }

        public override EntityState GetStateUpdate()
        {
            TState newState = (TState) base.GetStateUpdate();

            newState.Appearance = stateUpdate.Appearance;

            return newState;
        }

        public override void ApplyState(TState state)
        {
            base.ApplyState(state);

            title = state.Title ?? title;
            level = state.Level ?? level;
            name = state.Name ?? name;
            posture = state.Posture ?? posture;
            appearance = state.Appearance ?? appearance;
        }

        /// <summary>
        /// Sets the given appearance value.
        /// </summary>
        /// <param name="type">The type of appearance value to set</param>
        /// <param name="value">The value [0-100] of the appearance</param>
        public void SetAppearance(HumanoidAppearanceType type, int value = 0)
        {
            if (value > 100)
                value = 100;

            appearance[type] = value;

            stateUpdate.Appearance[type] = value;

            RegisterStateUpdate();
        }
    }
}
