﻿using MathNet.Spatial.Euclidean;
using System;

namespace GameServer.Entities
{
    public abstract class Entity<TState> : IEntity 
        where TState : EntityState, ICloneable, new()
    {
        protected Point3D position;
        protected float rotation;

        protected TState stateUpdate = new TState();
        protected EntityPosition positionUpdate = new EntityPosition();

        public Guid Id { get; private set; } = Guid.NewGuid();
        public Point3D Position
        {
            get => position;
            set
            {
                position = value;
                positionUpdate.Position = value;
                RegisterPositionUpdate();
            }
        }

        public float Rotation
        {
            get => rotation;
            set
            {
                rotation = value;
                positionUpdate.Rotation = value;
                RegisterPositionUpdate();
            }
        }

        public bool StateChanged { get; set; } = false;
        public bool PositionChanged { get; set; } = false;

        public virtual EntityState GetState()
        {
            TState state = new TState();

            state.Id = Id;
            state.Position = Position;
            state.Rotation = Rotation;

            return state;
        }

        public virtual EntityState GetStateUpdate()
        {
            TState newState;

            lock(stateUpdate)
            {
                newState = (TState)stateUpdate.Clone();

                StateChanged = false;

                stateUpdate = new TState();
            }

            newState.Id = Id;
            return newState;
        }

        public virtual EntityPosition GetPosition()
        {
            PositionChanged = false;
            return new EntityPosition() { Position = position, Rotation = rotation };
        }

        public virtual void ApplyState(TState state)
        {
            position = state.Position ?? position;
            rotation = state.Rotation ?? rotation;
        }

        protected void RegisterStateUpdate()
        {
            StateChanged = true;
        }

        protected void RegisterPositionUpdate()
        {
            PositionChanged = true;
        }
    }
}
