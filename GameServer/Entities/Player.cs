﻿namespace GameServer.Entities
{
    public class Player : Humanoid<PlayerState>
    {
        private PlayerPresence presence;

        public PlayerPresence Presence
        {
            get => presence;
            set
            {
                presence = value;
                stateUpdate.Presence = value;
                RegisterStateUpdate();
            }
        }

        public override void ApplyState(PlayerState state)
        {
            base.ApplyState(state);

            presence = state.Presence ?? presence;
        }
    }
}
