﻿using AutoMapper;
using System.Linq;

namespace GameServer.Entities
{
    class EntityProfile : Profile
    {
        public EntityProfile()
        {
            CreateMap<Player, PlayerState>();
        }
    }
}
