﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GameServer.Components
{
    public interface IComponent
    {
        void Initialize(MMOServer context);

        void Tick(object source, ElapsedEventArgs e);
    }
}
