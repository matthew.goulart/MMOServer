﻿using MathNet.Spatial.Euclidean;
using System.Collections.Generic;
using System.Linq;

namespace GameServer.Components.World
{
    public class CollisionMap
    {
        public List<Polygon2D> colliders;

        public bool IsColliding(Point2D position)
        {
            return colliders.Any(c => c.EnclosesPoint(position) == true);
        }
    }
}
