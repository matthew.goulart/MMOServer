﻿using GameServer.Components.Messaging.Service;
using GameServer.Entities;
using GameServer.Messages;
using MathNet.Spatial.Euclidean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameServer.Components.World
{
    public partial class WorldManager : Component, IWorldManager
    {
        /// <summary>
        /// Handles spawn requests from clients. If the player has never played before, it will send the default zone's default position, otherwise
        /// the player's last persisted position and zone is used
        /// </summary>
        /// <param name="client"></param>
        /// <param name="request"></param>
        [Handles(typeof(PlayerSpawnRequest))]
        public void HandlePlayerSpawnRequest(MmoClientSession client, PlayerSpawnRequest request)
        {
            var state = new PlayerState();

            //Get the character's initial state from storage, or create the default state in case of a new character
            var spawnZone = zones.FirstOrDefault();
            state.Position = new Point3D(10, 4, 0);
            state.Rotation = 0;

            //Set the initial values on the client
            Player character = client.Player = new Player();
            client.Zone = spawnZone;

            //Add the player to the zone
            spawnZone.AddEntity(character);

            //Apply the initial state to the Player instance
            character.ApplyState(state);

            //Prepare the response containing the initial state of the character
            var response = request.GetReply<PlayerSpawnResponse>();

            response.SpawnZone = spawnZone.Id;
            response.InitialState = state;
            response.TickRateMs = MMOServer.CLIENT_TICKRATE;

            context.SendTcp(client, response);
        }

        [Handles(typeof(PlayerReady))]
        public void HandlePlayerReady(MmoClientSession sender, PlayerReady message)
        {
            //Add the client to the list of "active" (spawned) players
            //The client will bgin receiving state and position updates
            //as of the next tick
            clients.TryAdd(sender.Id, sender);
        }
    }
}
