﻿using GameServer.Entities;
using MathNet.Spatial.Euclidean;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Xml.Serialization;

namespace GameServer.Components.World
{
    public class Zone
    {
        private string name;
        private Polygon2D bounds;
        private CollisionMap collisionMap;
        private HeightMap heightMap;

        private float VisibilityRange = 50;       

        public Zone()
        {

        }

        public Zone(string name, Polygon2D bounds, CollisionMap collisionMap)
        {
            this.name = name;
            this.bounds = bounds;
            this.collisionMap = collisionMap;

            Entities = new ConcurrentDictionary<Guid, IEntity>();
        }

        public Guid Id { get; set; }

        public ConcurrentDictionary<Guid, IEntity> Entities { get; }
        
        public static Zone LoadFromXml(string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Zone));
            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                return (Zone)serializer.Deserialize(fileStream);
            }
        }

        public bool ValidatePosition(Point3D position)
        {
            Point2D position2D = new Point2D(position.X, position.Z);
            return
                bounds.EnclosesPoint(position2D) && //Check if the position is within the outer bounds of the zone
                !collisionMap.IsColliding(position2D); //Check if the position collides with any of the zone's colliders
                //heightMap.ValidatePosition(position); //Check if the position is at am allowed height at the given cordinates
        }

        public List<IEntity> GetVisibleEntities(Player target)
        {
            if (!Entities.ContainsKey(target.Id))
                throw new ArgumentException("Player is not in this zone");

            return Entities.Values.Where(e => e != target && e.Position.DistanceTo(target.Position) <= VisibilityRange).ToList();
        }

        public List<Player> GetVisiblePlayers(Player target)
        {
            List<Player> visiblePlayers = new List<Player>();

            foreach (Player player in Entities.Values.Where(e => e is Player))
            {
                if (player == target)
                    continue;

                if (player.Position.DistanceTo(target.Position) <= VisibilityRange)
                    visiblePlayers.Add(player);
            }
            return visiblePlayers;
        }

        public List<Npc> GetVisibleNpcs(Player target)
        {
            List<Npc> visibleNpcs = new List<Npc>();

            foreach (Npc npc in Entities.Values.Where(e => e is Npc))
            {
                if (npc.Position.DistanceTo(target.Position) <= VisibilityRange)
                    visibleNpcs.Add(npc);
            }
            return visibleNpcs;
        }

        public List<Inanimate> GetVisibleInanimates(Player target)
        {
            List<Inanimate> visibleInanimates = new List<Inanimate>();

            foreach (Inanimate inanimate in Entities.Values.Where(e => e is Inanimate))
            {
                if (inanimate.Position.DistanceTo(target.Position) <= VisibilityRange)
                    visibleInanimates.Add(inanimate);
            }
            return visibleInanimates;
        }

        public void AddEntity(IEntity entity)
        {
            Entities.TryAdd(entity.Id, entity);
        }

        public void RemoveEntity(IEntity entity)
        {
            RemoveEntity(entity.Id);
        }

        public void RemoveEntity(Guid id)
        {
            Entities.TryRemove(id, out IEntity entity);
        }
    }
}
