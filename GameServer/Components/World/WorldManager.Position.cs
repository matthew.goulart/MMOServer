﻿using GameServer.Components.Messaging.Service;
using GameServer.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Components.World
{
    public partial class WorldManager : Component, IWorldManager
    {
        [Handles(typeof(PlayerPositionUpdate))]
        public void HandlePlayerPositionUpdate(MmoClientSession sender, PlayerPositionUpdate message)
        {
            if (message.Position.HasValue && sender.Zone.ValidatePosition(message.Position.Value))
                sender.Player.Position = message.Position.Value;

            if (message.Rotation.HasValue)
                sender.Player.Rotation = message.Rotation.Value;
        }
    }
}
