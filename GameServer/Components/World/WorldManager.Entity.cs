﻿using GameServer.Components.Messaging.Service;
using GameServer.Entities;
using GameServer.Messages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameServer.Components.World
{
    public partial class WorldManager : Component, IWorldManager
    {
        //Used to cache entity states during state updates
        private Dictionary<Guid, EntityState> entityStateCache = new Dictionary<Guid, EntityState>();
        private Dictionary<Guid, EntityPosition> entityPositionCache = new Dictionary<Guid, EntityPosition>();

        /// <summary>
        /// Generates state and position updates for each entity's visible peers in the given zone
        /// </summary>
        /// <param name="zone">The zone in which to perform generate updates</param>
        public void UpdateVisibleEntities(Zone zone)
        {
            foreach (MmoClientSession client in clients.Values.Where(c => c.Zone == zone))
            {
                log.LogTrace($"Starting Visible Entities Update for {client.Id}");

                EntityStateUpdates stateUpdatesMessage = null;
                EntityPositionUpdates positionUpdatesMessage = null;
                SpawnEnities spawnEntitiesMessage = null;

                List<IEntity> visibleEntities = zone.GetVisibleEntities(client.Player);

                log.LogTrace($"Found {visibleEntities.Count} visible entities for {client.Id}");

                foreach (IEntity entity in visibleEntities)
                {
                    if (client.trackedEntities.Contains(entity.Id))
                    {
                        log.LogTrace($"{client.Id} is alrady tracking this entity");

                        if (TryGetEntityStateUpdate(client, entity, out EntityState stateUpdate))
                        {
                            log.LogTrace($"Entity state update is available");

                            if (stateUpdatesMessage == null)
                                stateUpdatesMessage = new EntityStateUpdates();

                            stateUpdatesMessage.EntityStates.Add(stateUpdate);
                        }


                        if (TryGetEntityPositionUpdate(client, entity, out EntityPosition positionUpdate))
                        {
                            log.LogTrace($"Entity position update is available");

                            if (positionUpdatesMessage == null)
                                positionUpdatesMessage = new EntityPositionUpdates();

                            positionUpdatesMessage.EntityPositions.Add(entity.Id, positionUpdate);
                        }

                    }
                    else
                    {
                        log.LogTrace($"{client.Id} is not tracking this entity, getting full state...");

                        if (spawnEntitiesMessage == null)
                            spawnEntitiesMessage = new SpawnEnities();

                        spawnEntitiesMessage.NewEntities.Add(entity.GetState());
                        client.trackedEntities.Add(entity.Id);
                    }

                }

                //Send the updates!
                if (stateUpdatesMessage != null)
                {
                    log.LogTrace($"Sending states for {stateUpdatesMessage.EntityStates.Count} clients to {client.Id}");
                    context.SendTcp(client, stateUpdatesMessage);
                }


                if (positionUpdatesMessage != null)
                {
                    log.LogTrace($"Sending positions for {positionUpdatesMessage.EntityPositions.Count} clients to {client.Id}");
                    context.SendTcp(client, positionUpdatesMessage);
                }


                if (spawnEntitiesMessage != null)
                {
                    log.LogTrace($"Sending {spawnEntitiesMessage.NewEntities.Count} new entities to {client.Id}");
                    context.SendTcp(client, spawnEntitiesMessage);
                }

            }

            //Clear the caches for the next update
            entityStateCache.Clear();
            entityPositionCache.Clear();
        }

        /// <summary>
        /// Attempts to generate an <see cref="EntityState"/> containing the updated state of a given entity
        /// for the given <see cref="MmoClientSession"/>
        /// </summary>
        /// <param name="client">The client who will receive the update</param>
        /// <param name="entity">The entity being updated</param>
        /// <param name="state">The <see cref="EntityState"/> with the updated state info if available</param>
        /// <returns>True if the given <see cref="IEntity"/>'s state has changed since the last update, false if no update is required.</returns>
        private bool TryGetEntityStateUpdate(MmoClientSession client, IEntity entity, out EntityState state)
        {
            if (entityStateCache.TryGetValue(entity.Id, out state))
            {
                return true;
            }
            else if (entity.StateChanged)
            {
                state = entity.GetStateUpdate();
                entityStateCache.Add(entity.Id, state);
                return true;
            }

            state = default;
            return false;
        }

        private bool TryGetEntityPositionUpdate(MmoClientSession client, IEntity entity, out EntityPosition position)
        {
            if (entityPositionCache.TryGetValue(entity.Id, out position))
            {
                return true;
            }
            else if (entity.PositionChanged)
            {
                position = entity.GetPosition();
                entityPositionCache.Add(entity.Id, position);
                return true;
            }

            position = default;
            return false;
        }

        /// <summary>
        /// Handles State updates from clients (Position, rotation, posture, etc). Performs validation on positions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        [Handles(typeof(PlayerStateUpdate))]
        public void HandlePlayerStateUpdate(MmoClientSession sender, PlayerStateUpdate message)
        {
            sender.Player.ApplyState(message.State);
        }
    }
}
