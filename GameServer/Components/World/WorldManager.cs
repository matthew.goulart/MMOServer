﻿using GameServer.Components.Messaging.Handler;
using GameServer.Components.Messaging.Service;
using GameServer.Entities;
using GameServer.Messages;
using MathNet.Spatial.Euclidean;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace GameServer.Components.World
{
    public partial class WorldManager : Component, IWorldManager
    {
        private readonly ILogger log;
        private readonly IMessageService msgService;
        private readonly ITypedMessageHandler msgHandler;

        private List<Zone> zones = new List<Zone>();

        private ConcurrentDictionary<Guid, MmoClientSession> clients = new ConcurrentDictionary<Guid, MmoClientSession>();

        private bool ticking;

        public WorldManager(ILogger log, IMessageService msgService, ITypedMessageHandler msgHandler)
        {
            this.log = log;
            this.msgService = msgService;
            this.msgHandler = msgHandler;

            log.LogInformation("Created World Manager Component");

            msgService.SubscribeAll(this);

            //Delete this line if you don't want the test zone and entities to be generated programmatically.
            SetUpTestData();

        }

        ~WorldManager()
        {
            log.LogInformation("Beginning world shutdown");
        }

        public override void Tick(object source, ElapsedEventArgs e)
        {
            if(ticking)
            {
                log.LogWarning("Can't keep up! Last tick lasted longer than the tickrate.");
                return;
            }

            ticking = true;

            var stopwatch = Stopwatch.StartNew();

            try
            {
                Parallel.ForEach(zones, zone =>
                {
                    UpdateVisibleEntities(zone);
                });
            }
            finally
            {
                ticking = false;
            }

            stopwatch.Stop();

            log.LogTrace($"Tick took {stopwatch.ElapsedMilliseconds}ms");
        }

        /// <summary>
        /// Adds the zone to the instance collection and performs the required set-up
        /// </summary>
        /// <param name="zone"></param>
        public Zone RegisterZone(Zone zone)
        {
            zones.Add(zone);

            return zone;
        }



        #region Message Handlers
        /// <summary>
        /// Handles a player disconnect. This method should perform ALL of the necessary
        /// cleanup. I.E. removing the player from their current zone, the active clients list, etc...
        /// </summary>
        /// <param name="sender">The client who is disconnecting</param>
        /// <param name="message">The diconnect message (empty)</param>
        [Handles(typeof(PlayerDisconnected))]
        public void HandlePlayerDisconnected(MmoClientSession sender, PlayerDisconnected message)
        {
            sender.Zone?.RemoveEntity(sender.Player);
            clients.TryRemove(sender.Id, out MmoClientSession client);
        }
        #endregion
    }

}
