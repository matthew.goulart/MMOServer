﻿using GameServer.Entities;
using MathNet.Spatial.Euclidean;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Components.World
{
    public partial class WorldManager
    {
        private void SetUpTestData()
        {
            //Set up test zone
            CollisionMap testCollisionMap = new CollisionMap()
            {
                colliders = new List<Polygon2D>() {
                    new Polygon2D(
                        new Point2D[] {
                            new Point2D(22.5, 22.5),
                            new Point2D(27.5, 22.5),
                            new Point2D(27.5, 27.5),
                            new Point2D(22.5, 27.5)}) }
            };

            HeightMap testHeightMap = new HeightMap();

            Polygon2D testBounds = new Polygon2D(new Point2D[]
            {
                new Point2D(0,0),
                new Point2D(50,0),
                new Point2D(50,50),
                new Point2D(0,50)
            });

            var zone = RegisterZone(new Zone("TestZone", testBounds, testCollisionMap));

            Npc testNpc = new Npc() { Position = new Point3D(25, 0, 20), Rotation = 90, Name = "Test NPC", Level = 12 };
            testNpc.SetAppearance(HumanoidAppearanceType.NOSE_LENGTH, 100);
            testNpc.SetAppearance(HumanoidAppearanceType.NOSE_WIDTH, 100);

            Npc testNpc2 = new Npc() { Position = new Point3D(30, 0, 20), Rotation = 270, Name = "Test NPC 2", Level = 11 };

            zone.Entities.TryAdd(testNpc.Id, testNpc);
            zone.Entities.TryAdd(testNpc2.Id, testNpc2);
        }
    }
}
