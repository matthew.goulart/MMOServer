﻿using GameServer.Messages;
using RocketSocket.Message;
using System;
using System.Reflection;

namespace GameServer.Components.Messaging.Handler
{
    public interface ITypedMessageHandler : ITypedMessageHandler<int>
    { }

    public interface ITypedMessageHandler<TMessageId> : IMessageHandler<Message>
    {
        void RegisterMessagesInAssembly(Assembly assy);
        void RegisterAttributedTypes(Type[] types);
        void RegisterAttributedType(Type type);
        void RegisterType(TMessageId messageId, Type type);
    }
}
