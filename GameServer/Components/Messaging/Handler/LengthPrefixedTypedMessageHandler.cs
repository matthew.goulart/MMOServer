﻿using GameServer.Components.Messaging.Serializer;
using GameServer.Messages;
using GameServer.Utility;
using Microsoft.Extensions.Logging;
using RocketSocket.Client;
using RocketSocket.Message;
using RocketSocket.Utility.Buffer;
using System;
using System.Buffers;
using System.Buffers.Binary;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace GameServer.Components.Messaging.Handler
{
    public class LengthPrefixedTypedMessageHandler : ITypedMessageHandler
    {
        private readonly ILogger log;
        private readonly IMessageSerializer msgSerializer;

        private Dictionary<int, Tuple<Type, Delegate>> registeredMessages = new Dictionary<int, Tuple<Type, Delegate>>();

        const int LENGTH_PREFIX_WIDTH = 2;
        const int MESSAGE_ID_PREFIX_WIDTH = 2;
        const int CLIENT_ID_PREFIX_WIDTH = 22;
        const int STREAM_FRAME_HEADER_WIDTH = LENGTH_PREFIX_WIDTH + MESSAGE_ID_PREFIX_WIDTH;
        const int DATAGRAM_FRAME_HEADER_WIDTH = LENGTH_PREFIX_WIDTH + MESSAGE_ID_PREFIX_WIDTH + CLIENT_ID_PREFIX_WIDTH;

        public LengthPrefixedTypedMessageHandler(ILogger log, IMessageSerializer msgSerializer)
        {
            this.log = log;
            this.msgSerializer = msgSerializer;
        }

        /// <summary>
        /// Searches the given assembly for occurances of <see cref="Type"/>s with exactly one occurance of <see cref="MessageObjectAttribute"/> and registers them with the handler
        /// </summary>
        /// <param name="assy">The <see cref="Assembly"/> to search</param>
        public void RegisterMessagesInAssembly(Assembly assy)
        {
            foreach(Type type in assy.GetTypes().OrderBy(t => t.Name))
                RegisterAttributedType(type);

            log.LogDebug($"Registered all messages in {assy.FullName}");
        }

        /// <summary>
        /// Registers multiple <see cref="Type"/>s with the message handler
        /// </summary>
        /// <param name="type">The <see cref="Type"/>s to register</param>
        /// <remarks>The given <see cref="Type"/>s must each have one occurance of <see cref="MessageObjectAttribute"/></remarks>
        public void RegisterAttributedTypes(Type[] types)
        {
            foreach (Type type in types)
                RegisterAttributedType(type);
        }

        /// <summary>
        /// Registers a <see cref="Type"/> with the message handler
        /// </summary>
        /// <param name="type">The <see cref="Type"/> to register</param>
        /// <remarks>The given <see cref="Type"/> must have one occurance of <see cref="MessageObjectAttribute"/></remarks>
        public void RegisterAttributedType(Type type)
        {
            object[] attribs = type.GetCustomAttributes(typeof(MessageObjectAttribute), true);

            if (attribs.Length == 1)
            {
                log.LogDebug($"Registering message '{type.Name}");
                registeredMessages.Add(((MessageObjectAttribute)attribs[0]).Id, new Tuple<Type, Delegate>(type, GetDeserializer(type)));
            }
        }

        //TODO implement RegisterType method
        /// <summary>
        /// Not yet implemented
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="type"></param>
        public void RegisterType(int messageId, Type type)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a delegate of a method that serializes a given type
        /// </summary>
        /// <param name="genericType">The <see cref="Type"/> of the object that the method serializes</param>
        /// <returns>The <see cref="Delegate"/> of the method that serializes the given <see cref="Type"/></returns>
        private Delegate GetDeserializer(Type genericType)
        {
            var del = typeof(Func<,>).MakeGenericType(typeof(byte[]), genericType);
            return msgSerializer.GetType().GetMethod("Deserialize").MakeGenericMethod(genericType).CreateDelegate(del, msgSerializer);
        }


        /// <summary>
        /// Prepares a <see cref="Message"/> by generating a packet header and serializing the message.
        /// </summary>
        /// <param name="message">The <see cref="Message"/> to serialize</param>
        /// <param name="buffer">The <see cref="Span{T}"/> write the header and message to</param>
        /// <param name="bytesWritten">The total number of bytes written to the buffer</param>
        public void Prepare(Message message, Span<byte> buffer, out int bytesWritten)
        {
            byte[] content = msgSerializer.Serialize(message);

            content.CopyTo(buffer.Slice(STREAM_FRAME_HEADER_WIDTH));

            BinaryPrimitives.WriteInt16BigEndian(buffer, (short)content.Length);
            BinaryPrimitives.WriteInt16BigEndian(buffer.Slice(LENGTH_PREFIX_WIDTH), (short)registeredMessages.First(x => x.Value.Item1 == message.GetType()).Key);

            bytesWritten = STREAM_FRAME_HEADER_WIDTH + content.Length;
        }

        /// <summary>
        /// Attemps to parse a message received over TCP
        /// </summary>
        /// <param name="buffer">The buffer containing the received message</param>
        /// <param name="message">If succesful, will be set to the deserialized <see cref="Message"/></param>
        /// <param name="advancedTo">The position in the buffer to advance to.</param>
        /// <returns>True if a message was parsed, false if not.</returns>
        public bool TryParseStream(ReadOnlySequence<byte> buffer, out Message message, out SequencePosition advancedTo)
        {
            long bytesReceived = buffer.Length;

            if (bytesReceived < STREAM_FRAME_HEADER_WIDTH)
            {
                message = null;
                advancedTo = buffer.Start;
                return false;
            }

            short messageLength;
            short messageId;

            if (buffer.First.Length >= STREAM_FRAME_HEADER_WIDTH)
                messageLength= ParseStreamFrameHeader(buffer.First.Span, out messageId);
            else
            {
                Span<byte> local = stackalloc byte[STREAM_FRAME_HEADER_WIDTH];
                buffer.Slice(0, STREAM_FRAME_HEADER_WIDTH).CopyTo(local);
                messageLength = ParseStreamFrameHeader(local, out messageId);
            }

            if (bytesReceived - STREAM_FRAME_HEADER_WIDTH >= messageLength)
            {
                byte[] content;

                if(buffer.First.Length >= messageLength - STREAM_FRAME_HEADER_WIDTH)
                    content = buffer.First.Slice(STREAM_FRAME_HEADER_WIDTH, messageLength).ToArray();
                else
                {
                    Span<byte> local = stackalloc byte[messageLength];
                    buffer.Slice(STREAM_FRAME_HEADER_WIDTH, messageLength).CopyTo(local);
                    content = local.ToArray();
                }

                message = (Message)registeredMessages[messageId].Item2.DynamicInvoke(new object[] { content });

                advancedTo = buffer.GetPosition(STREAM_FRAME_HEADER_WIDTH + messageLength);

                return true;
            }
            else
            {
                message = null;
                advancedTo = buffer.Start;
                return false;
            }
        }

        /// <summary>
        /// Attemps to parse a message recceived over UDP
        /// </summary>
        /// <param name="datagram">The <see cref="Memory{T}"/> containing the message</param>
        /// <param name="message">If succesful, will be set to the deserialized message</param>
        /// <param name="clientId">If succesful, will be set to the client ID contained in the header</param>
        /// <returns>True if a message was parsed, false if not.</returns>
        public bool TryParseDatagram(Memory<byte> datagram, out Message message, out Guid? clientId)
        {
            clientId = default;

            long bytesReceived = datagram.Length;

            if (bytesReceived < DATAGRAM_FRAME_HEADER_WIDTH)
            {
                message = null;
                clientId = default;
                return false;
            }

            short messageLength;
            short messageId;

            messageLength = ParseDatagramFrameHeader(datagram, out clientId, out messageId);

            if (bytesReceived - STREAM_FRAME_HEADER_WIDTH >= messageLength)
            {
                byte[] content = datagram.Slice(DATAGRAM_FRAME_HEADER_WIDTH).ToArray();

                message = (Message)registeredMessages[messageId].Item2.DynamicInvoke(new object[] { content });

                return true;
            }
            else
            {
                message = null;
                return false;
            }
        }

        /// <summary>
        /// Extracts information from a TCP header
        /// </summary>
        /// <param name="bytes">The header data</param>
        /// <param name="id">The message ID</param>
        /// <returns>The length of the message payload</returns>
        private short ParseStreamFrameHeader(ReadOnlySpan<byte> bytes, out short id)
        {

            short length = BinaryPrimitives.ReadInt16BigEndian(bytes);

            id = BinaryPrimitives.ReadInt16BigEndian(bytes.Slice(2));

            return length;
        }

        /// <summary>
        /// Extracts information from a UDP header
        /// </summary>
        /// <param name="bytes">The header data</param>
        /// <param name="clientId">The extracted client ID</param>
        /// <param name="messageId">The message ID</param>
        /// <returns></returns>
        private short ParseDatagramFrameHeader(Memory<byte> bytes, out Guid? clientId, out short messageId)
        {
            Span<byte> span = bytes.Span;

            short length = BinaryPrimitives.ReadInt16BigEndian(span);
            messageId = BinaryPrimitives.ReadInt16BigEndian(span.Slice(2));
            clientId = ShortGuid.Decode(bytes.Slice(4).ToString());

            return length;
        }
    }
}
