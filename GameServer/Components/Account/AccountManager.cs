﻿using GameServer.Components.Messaging.Handler;
using GameServer.Components.Messaging.Service;
using GameServer.Messages;
using Microsoft.Extensions.Logging;
using System.Timers;

namespace GameServer.Components.Account
{
    class AccountManager : IAccountManager
    {
        private readonly ILogger log;
        private readonly IMessageService msgService;
        private readonly ITypedMessageHandler msgHandler;
        private MMOServer context;

        public AccountManager(
            ILogger log, 
            IMessageService msgService, 
            ITypedMessageHandler msgHandler)
        {
            this.log = log;
            this.msgService = msgService;
            this.msgHandler = msgHandler;

            msgService.SubscribeAll(this);
        }

        public void Initialize(MMOServer context)
        {
            this.context = context;
        }

        public void Tick(object source, ElapsedEventArgs e)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="request"></param>
        [Handles(typeof(LoginRequest))]
        void HandleLoginRequest(MmoClientSession sender, LoginRequest request)
        {
            var message = request.GetReply<LoginResponse>();

            if (request.Username == "test" && request.Password == "test")
            {
                message.Result = true;
                message.ConnectTo = "Yo mamma";
            }
            else
            {
                message.Result = false;
                message.Error = "Invalid credentials";
            }

            context.SendTcp(sender, message);
        }
    }
}
