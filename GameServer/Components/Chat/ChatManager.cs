﻿using GameServer.Components.Messaging.Service;
using GameServer.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace GameServer.Components.Chat
{
    class ChatManager : Component
    {
        public override void Tick(object source, ElapsedEventArgs e)
        {
            
        }

        [Handles(typeof(DialogueRequest))]
        private DialogueReply HandleDialogueRequest(MmoClientSession client, DialogueRequest message)
        {
            var reply = message.GetReply<DialogueReply>();

            reply.Dialogue = "Testing!";

            return reply;
        }
    }
}
