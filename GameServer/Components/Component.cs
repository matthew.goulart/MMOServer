﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GameServer.Components
{
    public abstract class Component
    {
        protected MMOServer context;

        public virtual void Initialize(MMOServer context)
        {
            this.context = context;
        }

        public abstract void Tick(object source, ElapsedEventArgs e);
    }
}
