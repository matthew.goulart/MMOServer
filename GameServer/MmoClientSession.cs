﻿using GameServer.Components.World;
using GameServer.Entities;
using RocketSocket.Client;
using System;
using System.Collections.Generic;

namespace GameServer
{
    public class MmoClientSession : ClientSession
    {
        public int AccountId { get; set; }
        
        public bool IsSpawned { get; set; }
        public Player Player { get; set; }
        public Zone Zone { get; set; }
        public List<Guid> trackedEntities { get; set; } = new List<Guid>();
    }
}
