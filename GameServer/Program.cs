﻿using GameServer.Components.Messaging.Handler;
using GameServer.Entities;
using Microsoft.Extensions.Configuration;
using Ninject;
using Ninject.Parameters;
using RocketSocket;
using System;
using System.Net;

namespace GameServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Automapper
            AutoMapper.Mapper.Initialize(cfg => cfg.AddProfile<EntityProfile>());

            //Add ninject modules here to add dependencies.
            //Each Component should have it's own module.
            IKernel kernel = new StandardKernel(new Services());

            RocketServerConfiguration serverCfg = new RocketServerConfiguration()
                .ListenTcp((tcp) =>
                {
                    tcp
                    .On(IPAddress.Any)
                    .Port(5816)
                    .UseNagle(true);
                })
                .ListenUdp((udp) =>
                {
                    udp
                    .On(IPAddress.Any)
                    .Port(5817);
                })
                .MaximumClients(2000)
                .MaximumMessageLength(2048)
                .HandleMessagesWith(kernel.Get<ITypedMessageHandler>());

            //Get an instance of MMOServer with dependencies recursively injected
            var config = new ConstructorArgument("config", serverCfg);
            MMOServer server = kernel.Get<MMOServer>(config);

            server.Start();

            //Wait for exit
            Console.ReadLine();

            //Shutdown NLog
            NLog.LogManager.Shutdown();
        }



    }
}
