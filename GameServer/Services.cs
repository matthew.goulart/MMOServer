﻿using GameServer.Components.Account;
using GameServer.Components.Messaging.Handler;
using GameServer.Components.Messaging.Serializer;
using GameServer.Components.Messaging.Service;
using GameServer.Components.World;
using Microsoft.Extensions.Logging;
using Ninject.Modules;
using NLog.Extensions.Logging;

namespace GameServer
{
    class Services : NinjectModule
    {
        public override void Load()
        {
            //Logging            
            ILoggerFactory loggerFactory = new LoggerFactory()
                .AddNLog();

            NLog.LogManager.LoadConfiguration("nlog.config");

            //Service bindings
            Bind<ILogger>().ToMethod(x =>
            {
                if (x.Request.Target == null)
                    return loggerFactory.CreateLogger("MMO Server");
                else
                    return loggerFactory.CreateLogger(x.Request.Target.Member.DeclaringType.Name);
            });
            Bind<ITypedMessageHandler>().To<LengthPrefixedTypedMessageHandler>().InSingletonScope();
            Bind<IMessageService>().To<MessageService>().InSingletonScope();
            Bind<IMessageSerializer>().To<MessagePackMessageSerializer>();
            Bind<IWorldManager>().To<WorldManager>().InSingletonScope();
            Bind<IAccountManager>().To<AccountManager>().InSingletonScope();
        }
    }
}
