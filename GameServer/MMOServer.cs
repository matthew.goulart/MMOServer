﻿using GameServer.Components;
using GameServer.Components.Account;
using GameServer.Components.Messaging.Handler;
using GameServer.Components.Messaging.Service;
using GameServer.Components.World;
using GameServer.Messages;
using Microsoft.Extensions.Logging;
using RocketSocket;
using RocketSocket.Connection;
using RocketSocket.Connection.TCP;
using RocketSocket.Connection.UDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;

namespace GameServer
{
    public class MMOServer : RocketServer<MmoClientSession,  Message>
    {
        public const int SERVER_TICKRATE = 250;
        public const int CLIENT_TICKRATE = 250;

        //Components
        private List<IComponent> components = new List<IComponent>();

        //Place dependencies here
        private readonly ILogger log;
        private readonly IMessageService msgService;
        private readonly IWorldManager worldManager;
        private readonly IAccountManager accntManager;

        //Timekeeping
        private Timer tickTimer;

        public MMOServer(
            RocketServerConfiguration config,
            ITypedMessageHandler msgHandler,
            ILogger log,
            IMessageService msgService,
            IWorldManager worldManager,
            IAccountManager accountManager
            )
            :base(config, log)
        {
            //Set up the timer
            tickTimer = new Timer(SERVER_TICKRATE);
            tickTimer.Elapsed += ServerTick;
            tickTimer.Start();

            //Assign dependencies
            this.log        = log;
            this.msgService = msgService;

            //Assign and register (initialize) components
            worldManager = RegisterComponent(worldManager);
            accntManager = RegisterComponent(accountManager);

            //Prepare the message service and handler
            msgService.SubscribeAll(this);
            msgHandler.RegisterMessagesInAssembly(Assembly.GetAssembly(typeof(Message)));

            log.LogInformation("Created MMO Play Server instance");            
        }

        /// <summary>
        /// Initializes a component, providing it the server context and adds it to the component list for tracking
        /// </summary>
        /// <typeparam name="T">The type of the component</typeparam>
        /// <param name="component">The component</param>
        /// <returns>The initialized component</returns>
        private T RegisterComponent<T>(T component) where T : IComponent
        {
            component.Initialize(this);
            tickTimer.Elapsed += component.Tick;

            components.Add(component);

            return component;
        }

        #region RocketServer implementation

        private void ServerTick(object source, ElapsedEventArgs e)
        {

        }

        protected override void OnStart()
        {
            log.LogInformation("Starting listeners...");

            foreach (RocketSocket.Connection.TCP.TcpListener listener in TcpListeners)
                log.LogInformation($"Listening on {listener.LocalEndPoint.Address}:{listener.LocalEndPoint.Port} ({listener.ProtocolType})");

            foreach (UdpListener listener in UdpListeners)
                log.LogInformation($"Listening on {listener.LocalEndPoint.Address}:{listener.LocalEndPoint.Port} ({listener.ProtocolType})");
        }

        protected override void OnMessageReceived(MmoClientSession client, Message message)
        {
            msgService.Send(message, client);
        }

        protected override void OnClientDisconnecting(MmoClientSession client)
        {
            msgService.Send(new PlayerDisconnected(), client);
        }

        public async Task<TReply> SendWithReplyAsync<TReply, TMessage>(MmoClientSession client, TMessage message, int timeout = 60000) where TReply : Message where TMessage : Message
        {
            var task = msgService.RegisterReplyHandler(message, timeout);

            await SendTcp(client, message);

            return (TReply)await task;
        }

        public async Task<TReply> SendWithReplyAsync<TReply, TMessage>(MmoClientSession client, TMessage message, UdpListener listener, string remoteEndPoint, int timeout = 60000) where TReply : Message where TMessage : Message
        {
            var task = msgService.RegisterReplyHandler(message, timeout);

            SendUdp(client, message, listener, remoteEndPoint);

            return (TReply)await task;
        }
        #endregion

        #region Message Handlers
        [Handles(typeof(StatisticsRequest))]
        private void StatisticsRequest(MmoClientSession sender, StatisticsRequest message)
        {
            var reply = message.GetReply<StatisticsReply>();
            reply.TotalBytesSent = TotalBytesSent;
            reply.TotalBytesReceived = TotalBytesReceived;
            reply.CurrentConnectedCliets = clientSessions.Count;
            reply.Uptime = TimeSpan.FromMinutes(134);
            reply.AverageLoad = 46;
                       
            SendTcp(sender, reply);
        }


        #endregion
    }
}
