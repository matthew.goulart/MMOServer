﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Data
{
    public class Account
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Character> Characters { get; set; }
    }
}
