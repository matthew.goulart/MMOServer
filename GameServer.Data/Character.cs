﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameServer.Data
{
    public class Character
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        //Position
        public Guid Zone { get; set; }
        public double ZoneX { get; set; }
        public double ZoneY { get; set; }
        public double ZoneZ { get; set; }
        public double Rotation { get; set; }
    }
}