﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameServer.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HeldLeftHandId = table.Column<Guid>(nullable: true),
                    HeldRightHandId = table.Column<Guid>(nullable: true),
                    LeftHandId = table.Column<Guid>(nullable: true),
                    RightHandId = table.Column<Guid>(nullable: true),
                    LeftArmId = table.Column<Guid>(nullable: true),
                    RightArmId = table.Column<Guid>(nullable: true),
                    LeftShoulderId = table.Column<Guid>(nullable: true),
                    RightShoulderId = table.Column<Guid>(nullable: true),
                    LeftFootId = table.Column<Guid>(nullable: true),
                    RightFootId = table.Column<Guid>(nullable: true),
                    LeftLegId = table.Column<Guid>(nullable: true),
                    RightLegId = table.Column<Guid>(nullable: true),
                    WaistId = table.Column<Guid>(nullable: true),
                    UpperBodyId = table.Column<Guid>(nullable: true),
                    HeadId = table.Column<Guid>(nullable: true),
                    FaceId = table.Column<Guid>(nullable: true),
                    Zone = table.Column<Guid>(nullable: false),
                    ZoneX = table.Column<double>(nullable: false),
                    ZoneY = table.Column<double>(nullable: false),
                    ZoneZ = table.Column<double>(nullable: false),
                    Rotation = table.Column<double>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Characters_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Item",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Rarity = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CharacterId = table.Column<Guid>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    Slot = table.Column<int>(nullable: true),
                    Quality = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Item", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Item_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Characters_AccountId",
                table: "Characters",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_FaceId",
                table: "Characters",
                column: "FaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_HeadId",
                table: "Characters",
                column: "HeadId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_HeldLeftHandId",
                table: "Characters",
                column: "HeldLeftHandId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_HeldRightHandId",
                table: "Characters",
                column: "HeldRightHandId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_LeftArmId",
                table: "Characters",
                column: "LeftArmId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_LeftFootId",
                table: "Characters",
                column: "LeftFootId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_LeftHandId",
                table: "Characters",
                column: "LeftHandId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_LeftLegId",
                table: "Characters",
                column: "LeftLegId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_LeftShoulderId",
                table: "Characters",
                column: "LeftShoulderId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_RightArmId",
                table: "Characters",
                column: "RightArmId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_RightFootId",
                table: "Characters",
                column: "RightFootId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_RightHandId",
                table: "Characters",
                column: "RightHandId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_RightLegId",
                table: "Characters",
                column: "RightLegId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_RightShoulderId",
                table: "Characters",
                column: "RightShoulderId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_UpperBodyId",
                table: "Characters",
                column: "UpperBodyId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_WaistId",
                table: "Characters",
                column: "WaistId");

            migrationBuilder.CreateIndex(
                name: "IX_Item_CharacterId",
                table: "Item",
                column: "CharacterId");

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_FaceId",
                table: "Characters",
                column: "FaceId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_HeadId",
                table: "Characters",
                column: "HeadId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_HeldLeftHandId",
                table: "Characters",
                column: "HeldLeftHandId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_HeldRightHandId",
                table: "Characters",
                column: "HeldRightHandId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_LeftArmId",
                table: "Characters",
                column: "LeftArmId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_LeftFootId",
                table: "Characters",
                column: "LeftFootId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_LeftHandId",
                table: "Characters",
                column: "LeftHandId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_LeftLegId",
                table: "Characters",
                column: "LeftLegId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_LeftShoulderId",
                table: "Characters",
                column: "LeftShoulderId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_RightArmId",
                table: "Characters",
                column: "RightArmId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_RightFootId",
                table: "Characters",
                column: "RightFootId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_RightHandId",
                table: "Characters",
                column: "RightHandId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_RightLegId",
                table: "Characters",
                column: "RightLegId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_RightShoulderId",
                table: "Characters",
                column: "RightShoulderId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_UpperBodyId",
                table: "Characters",
                column: "UpperBodyId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Item_WaistId",
                table: "Characters",
                column: "WaistId",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Accounts_AccountId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_FaceId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_HeadId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_HeldLeftHandId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_HeldRightHandId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_LeftArmId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_LeftFootId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_LeftHandId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_LeftLegId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_LeftShoulderId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_RightArmId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_RightFootId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_RightHandId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_RightLegId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_RightShoulderId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_UpperBodyId",
                table: "Characters");

            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Item_WaistId",
                table: "Characters");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Item");

            migrationBuilder.DropTable(
                name: "Characters");
        }
    }
}
