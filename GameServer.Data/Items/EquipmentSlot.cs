﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Data
{
    public enum EquipmentSlot
    {
        HeldLeftHand,
        HeldRightHand,
        LeftHand,
        RightHand,
        LeftArm,
        RightArm,
        LeftShoulder,
        RightShoulder,
        LeftFoot,
        RightFoot,
        LeftLeg,
        RightLeg,
        Waist,
        UpperBody,
        Head,
        Face
    }
}
