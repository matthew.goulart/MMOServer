﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Data
{
    public enum ItemRarity
    {
        Common,
        Uncommon,
        Rare,
        Legendary,
        Godly
    }
}
