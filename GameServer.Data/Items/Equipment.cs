﻿namespace GameServer.Data
{
    public class Equipment : Item
    {
        public EquipmentSlot Slot { get; set; }
    }
}