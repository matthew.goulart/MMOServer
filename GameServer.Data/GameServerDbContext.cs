﻿using Microsoft.EntityFrameworkCore;

namespace GameServer.Data
{
    public class GameServerDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=LAPTOP-HA4VRQK3\SQLEXPRESS;Initial Catalog=MmoServer;Integrated Security=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

        //Items
        public virtual DbSet<Item> Items { get; set; } 

        //Player
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Character> Characters { get; set; }
    }
}
