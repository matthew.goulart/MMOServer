﻿using MathNet.Spatial.Euclidean;
using UnityEngine;

namespace GameServer.UnityClient.Utility
{
    public static class Extensions
    {
        public static Vector3 ToVector3(this Point3D point)
        {
            return new Vector3((float)point.X, (float)point.Y, (float)point.Z);
        }

        public static Point3D ToPoint3D(this Vector3 vector)
        {
            return new Point3D(vector.x, vector.y, vector.z);
        }
    }
}
