﻿namespace Microsoft.Extensions.Logging
{
    public interface ILogger
    {
        void LogDebug(string message);
        void LogTrace(string message);
        void LogInformation(string message);
        void LogWarning(string message);
        void LogError(string message);
        void LogCritical(string message);
    }
}
