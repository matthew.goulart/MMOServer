﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameServer.UnityClient.Utility.Logger
{
    class UnityLoggerShim : Microsoft.Extensions.Logging.ILogger
    {
        public void LogCritical(string message)
        {
            Debug.LogError(message);
        }

        public void LogDebug(string message)
        {
            Debug.LogAssertion(message);
        }

        public void LogError(string message)
        {
            Debug.LogError(message);
        }

        public void LogInformation(string message)
        {
            Debug.Log(message);
        }

        public void LogTrace(string message)
        {
            Debug.Log(message);
        }

        public void LogWarning(string message)
        {
            Debug.LogWarning(message);
        }
    }
}
