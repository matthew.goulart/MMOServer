﻿using GameServer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.UnityClient.Scripts
{
    public interface INpcController
    {
        void ApplyState(NpcState state);
    }
}
