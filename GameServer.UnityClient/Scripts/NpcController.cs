﻿using GameServer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameServer.UnityClient.Scripts
{
    class NpcController : MonoBehaviour
    {
        public float baseRunSpeed = 3.0f;

        private Animator animator;

        private Vector3 desiredPosition;
        private Quaternion desiredRotation;
        private bool reachedTarget;
        private bool running;

        private float currentSpeed;
        private float acceleration;

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }

        void Start()
        {
            desiredPosition = gameObject.transform.position;

            animator = GetComponent<Animator>();
        }

        void Update()
        {
            //Movement
            float distanceToDesiredPosition = Vector3.Distance(gameObject.transform.position, desiredPosition);

            if (distanceToDesiredPosition > 0.1)
            {
                //Vector3 look = desiredPosition - transform.position;
                //Quaternion lookRotation = Quaternion.LookRotation(look);
                //transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, 10 * Time.deltaTime);

                if (currentSpeed < baseRunSpeed)
                {
                    currentSpeed = Mathf.Clamp(acceleration * Time.deltaTime, 0, baseRunSpeed);
                }

                gameObject.transform.LookAt(desiredPosition, Vector3.up);
                gameObject.transform.Translate(Vector3.forward * Time.deltaTime * baseRunSpeed);
                if (running == false && distanceToDesiredPosition >= 0.1)
                {
                    running = true;
                    animator.SetBool("running", true);
                }
                else
                {
                    running = false;
                    animator.SetBool("running", false);
                }

            }
            else if (transform.rotation != desiredRotation)
            {
                transform.rotation = Quaternion.LerpUnclamped(transform.rotation, desiredRotation, 5 * Time.deltaTime);
            }
        }

        public void Move(Vector3 position, Quaternion rotation)
        {
            desiredPosition = position;
            desiredRotation = rotation;
        }

        public void WalkTo(Vector3 position)
        {
            throw new NotImplementedException();
        }

        public void RunTo(Vector3 position)
        {
            throw new NotImplementedException();
        }

        public void SetPosture(HumanoidPosture posture)
        {
            throw new NotImplementedException();
        }

        public void SetPresence(PlayerPresence presence)
        {
            throw new NotImplementedException();
        }

        public void ChangeEquipment(EquipmentSlot slot, Guid equipmentId)
        {
            throw new NotImplementedException();
        }

        public void ApplyState(PlayerState state)
        {
            throw new NotImplementedException();
        }
    }
}
