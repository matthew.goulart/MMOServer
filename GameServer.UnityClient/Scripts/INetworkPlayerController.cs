﻿using GameServer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameServer.UnityClient.Scripts
{
    public interface INetworkPlayerController
    {
        void WalkTo(Vector3 position);
        void RunTo(Vector3 position);
        void SetPosture(HumanoidPosture posture);
        void SetPresence(PlayerPresence presence);
        void ChangeEquipment(EquipmentSlot slot, Guid equipmentId);

        void ApplyState(PlayerState state);
    }
}
