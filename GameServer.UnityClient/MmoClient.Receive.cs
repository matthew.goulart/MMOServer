﻿using GameServer.Messages;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.UnityClient
{
    public partial class MmoClient
    {
        /// <summary>
        /// Receive loop to asynchronously wait for and attempt to parse arriving data
        /// </summary>
        /// <param name="client"></param>
        protected async Task DoReceiveAsync()
        {
            if (!tcpClient.Connected)
                throw new InvalidOperationException("Not connected.");

            PipeReader reader = tcpClientPipe.Input;

            while (true)
            {
                ReadResult read = await reader.ReadAsync();

                if (read.IsCanceled)
                    break;

                ReadOnlySequence<byte> buffer = read.Buffer;

                if (buffer.IsEmpty && read.IsCompleted)
                    break;

                if (msgHandler.TryParse(buffer, out Message message, out SequencePosition advancedTo))
                {
                    reader.AdvanceTo(advancedTo);
                    MessageService.Send(message);
                }
                else
                    reader.AdvanceTo(buffer.Start, buffer.End);
            }
        }
    }
}
