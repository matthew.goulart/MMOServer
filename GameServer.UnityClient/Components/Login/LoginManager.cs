﻿using GameServer.Components.Messaging.Service;
using GameServer.Messages;
using GameServer.UnityClient.Components.Messaging;
using GameServer.UnityClient.Components.World;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameServer.UnityClient.Components.Login
{
    public class LoginManager : MonoBehaviour
    {

        private MessageService msgService;

        private MmoClient client;
        private WorldManager worldManager;

        public InputField usernameField;
        public InputField passwordField;

        // Use this for initialization
        void Start()
        {
            client = gameObject.GetComponent<MmoClient>();
            worldManager = gameObject.GetComponent<WorldManager>();

            msgService = MessageService.Instance;

            msgService.SubscribeAll(this);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public async void RequestServerStatistics()
        {
            var message = new StatisticsRequest();

            var reply = await client.SendWithReplyAsync<StatisticsReply>(message);

            Debug.Log($"Bytes received: {reply.TotalBytesReceived}, Bytes Sent: {reply.TotalBytesSent}, Connected Clients: {reply.CurrentConnectedCliets}");
        }


        //Login
        public async void RequestLogin()
        {
            var username = usernameField.text;
            var password = passwordField.text;

            var message = new LoginRequest() { Username = username, Password = password };

            var reply = await client.SendWithReplyAsync<LoginResponse>(message);

            if (reply.Result)
            {
                Debug.Log($"Login success!");
                worldManager.RequestSpawn();
    
            }
            else
            {
                Debug.Log($"Login failed! {reply.Error}");
            }
        }

        
    }

}
