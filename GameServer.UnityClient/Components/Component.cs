﻿namespace GameServer.UnityClient.Components
{
    public abstract class Component
    {
        protected MmoClient context;

        public void SetContext(MmoClient context)
        {
            this.context = context;
        }

        public abstract void Tick();
    }
}
