﻿namespace GameServer.UnityClient.Components
{
    interface IComponent
    {
        void Initialize();
    }
}
