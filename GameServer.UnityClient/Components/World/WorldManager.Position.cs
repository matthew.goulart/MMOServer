﻿using GameServer.Components.Messaging.Service;
using GameServer.Entities;
using GameServer.Messages;
using GameServer.UnityClient.Scripts;
using GameServer.UnityClient.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameServer.UnityClient.Components.World
{
    public partial class WorldManager : MonoBehaviour
    {
        /// <summary>
        /// Handles entity position updates from the server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        [Handles(typeof(EntityPositionUpdates))]
        public void HandleEntityPositionUpdates(object sender, EntityPositionUpdates message)
        {
            foreach(KeyValuePair<Guid, EntityPosition> position in message.EntityPositions)
            {
                if(trackedPlayers.TryGetValue(position.Key, out NetworkPlayerController player))
                {
                    player.Move(position.Value.Position.ToVector3(), Quaternion.AngleAxis(position.Value.Rotation, Vector3.up));
                    continue;
                }

                if(trackedNpcs.TryGetValue(position.Key, out GameObject npc))
                {
                    npc.transform.position = position.Value.Position.ToVector3();
                    npc.transform.rotation = Quaternion.Euler(0, position.Value.Rotation, 0);
                    continue;
                }

                if(trackedInanimates.TryGetValue(position.Key, out GameObject inanimate))
                {
                    inanimate.transform.position = position.Value.Position.ToVector3();
                    inanimate.transform.rotation = Quaternion.Euler(0, position.Value.Rotation, 0);
                    continue;
                }
                    
            }
        }
    }
}
