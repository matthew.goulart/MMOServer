﻿using GameServer.Components.Messaging.Service;
using GameServer.Entities;
using GameServer.Messages;
using GameServer.UnityClient.Entities;
using GameServer.UnityClient.Scripts;
using GameServer.UnityClient.Utility;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameServer.UnityClient.Components.World
{
    public partial class WorldManager : MonoBehaviour
    {
        public async Task RequestSpawn()
        {
            //TODO show loading screen here

            //Get the initial spawn zone and transform
            Debug.Log("Getting initial spawn zone and transform...");
            var spawnReply = await client.SendWithReplyAsync<PlayerSpawnResponse>(new PlayerSpawnRequest()).ConfigureAwait(true);

            //Load the correct zone
            Debug.Log("Loading zone...");
            await SceneManager.LoadSceneAsync("testingGround");

            //Create the local player
            var playerGO = Instantiate(localPlayerPrefab) as GameObject;
            player = playerGO.GetComponent<LocalPlayer>();

            //Move the player to the correct location
            player.ApplyState(spawnReply.InitialState);

            //Tell the server we are ready to start receiving player state and position updates
            await client.Send(new PlayerReady());

            //Wait a few seconds to allow the other players to
            //make their way into the local world
            await Task.Delay(2000);

            Debug.Log("Load done! Game starting...");

            //Last thing to do is start sending our own state/position to the server
            //using the supplied tickrate (in ms).
            InvokeRepeating("Tick", 0, (float)spawnReply.TickRateMs / 1000);

            //TODO hide loading screen here
        }

        private void SpawnPlayer(PlayerState state)
        {
            var player = Instantiate(
                playerPrefab,
                state.Position.Value.ToVector3(),
                Quaternion.Euler(0, state.Rotation.Value, 0))
                as GameObject;

            player.name = $"Player ({state.Id})";
            player.tag = "Player";

            player.GetComponentInChildren<TextMesh>().text = state.Name;

            trackedPlayers.Add(state.Id, player.GetComponent<NetworkPlayerController>());
        }

        private void SpawnNpc(NpcState state)
        {
            var npc = Instantiate(
                npcPrefab,
                state.Position.Value.ToVector3(),
                Quaternion.Euler(0, state.Rotation.Value, 0))
                as GameObject;

            npc.name = $"NPC ({state.Id})";
            npc.tag = "NPC";

            INpcController controller = npc.GetComponent<INpcController>();

            controller.ApplyState(state);

            trackedNpcs.Add(state.Id, npc);
        }

        private void SpawnInanimate(InanimateState state)
        {
            var inanimate = Instantiate(
                inanimatePrefab,
                state.Position.Value.ToVector3(),
                Quaternion.Euler(0, state.Rotation.Value, 0))
                as GameObject;

            inanimate.name = $"Inanimate ({state.Id})";
            inanimate.tag = "Inanimate";

            trackedInanimates.Add(state.Id, inanimate);
        }

        #region Message Handlers
        [Handles(typeof(SpawnEnities))]
        private void HandleNewEntities(object sender, SpawnEnities message)
        {
            foreach(EntityState initialState in message.NewEntities)
            {
                switch(initialState)
                {
                    case PlayerState playerState:
                        SpawnPlayer(playerState);
                        break;
                    case NpcState npcState:
                        SpawnNpc(npcState);
                        break;
                    case InanimateState inanimateState:
                        SpawnInanimate(inanimateState);
                        break;
                }
            }
        }

        [Handles(typeof(DestroyEntities))]
        private void HandleDestroyEntities(object sender, DestroyEntities message)
        {

        }
        #endregion
    }
}
