﻿using GameServer.Components.Messaging.Service;
using GameServer.Entities;
using GameServer.Messages;
using GameServer.UnityClient.Components.Messaging;
using GameServer.UnityClient.Entities;
using GameServer.UnityClient.Scripts;
using GameServer.UnityClient.Utility;
using MathNet.Spatial.Euclidean;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameServer.UnityClient.Components.World
{
    [RequireComponent(typeof(MmoClient))]
    public partial class WorldManager : MonoBehaviour
    {
        private Guid zoneId;
        private Dictionary<Guid, NetworkPlayerController> trackedPlayers = new Dictionary<Guid, NetworkPlayerController>();
        private Dictionary<Guid, GameObject> trackedNpcs = new Dictionary<Guid, GameObject>();
        private Dictionary<Guid, GameObject> trackedInanimates = new Dictionary<Guid, GameObject>();

        private MessageService msgService;
        private MmoClient client;
        private LocalPlayer player;

        public UnityEngine.Object localPlayerPrefab;
        public UnityEngine.Object playerPrefab;
        public UnityEngine.Object npcPrefab;
        public UnityEngine.Object inanimatePrefab;

        void Start()
        {
            client = gameObject.GetComponent<MmoClient>();

            msgService = MessageService.Instance;
            msgService.SubscribeAll(this);

            //Check that all prefabs have been set
            if (playerPrefab == null)
                throw new ArgumentNullException("The Player Prefab is not set.");

            if (npcPrefab == null)
                throw new ArgumentNullException("The NPC Prefab is not set.");

            if(inanimatePrefab == null)
                throw new ArgumentNullException("The Inanimate Object Prefab is not set.");
        }

        public void Tick()
        {
            if(player.StateChanged)
            {
                PlayerState stateUpdate = player.GetStateUpdate();
                client.Send(new PlayerStateUpdate() { State = stateUpdate });
            }

            if(player.PositionChanged)
            {
                player.PositionChanged = false;

                client.Send(new PlayerPositionUpdate()
                {
                    Position = player.Position.ToPoint3D(),
                    Rotation = player.Rotation
                });
            }
        }
    }
}
