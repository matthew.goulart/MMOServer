﻿using GameServer.Components.Messaging.Service;
using GameServer.Entities;
using GameServer.Messages;
using GameServer.UnityClient.Scripts;
using GameServer.UnityClient.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameServer.UnityClient.Components.World
{
    public partial class WorldManager : MonoBehaviour
    {
        /// <summary>
        /// Handles entity state updates from the server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        [Handles(typeof(EntityStateUpdates))]
        public void HandleEntityStateUpdates(object sender, EntityStateUpdates message)
        {
            foreach (EntityState state in message.EntityStates)
            {
                switch (state)
                {
                    case PlayerState playerState:
                        HandlePlayerStateUpdate(playerState);
                        break;
                    case NpcState npcState:
                        HandleNpcStateUpdate(npcState);
                        break;
                    case InanimateState inanimateState:
                        HandleInanimateStateUpdate(inanimateState);
                        break;
                }
            }
        }

        ///<summary>
        /// Applies any state changes to the given entity
        /// </summary>
        /// <param name="state"></param>
        private void HandlePlayerStateUpdate(PlayerState state)
        {
            if(trackedPlayers.TryGetValue(state.Id, out NetworkPlayerController player))
            {
                //TODO Handle state updates
            }
            else
            {
                //TODO Handle untracked entity error
            }
        }

        private void HandleNpcStateUpdate(NpcState state)
        {
            if(trackedNpcs.TryGetValue(state.Id, out GameObject Npc))
            {
                //TODO Handle state updates
            }
            else
            {
                //TODO Handle untracked entity error
            }
        }

        private void HandleInanimateStateUpdate(InanimateState state)
        {
            if(trackedInanimates.TryGetValue(state.Id, out GameObject inanimate))
            {
                //TODO Handle state updates
            }
            else
            {
                //TODO Handle untracked entity error
            }
        }
    }
}
