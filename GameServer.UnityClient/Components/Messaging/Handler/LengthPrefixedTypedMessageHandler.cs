﻿using GameServer.Components.Messaging.Serializer;
using GameServer.Messages;
using GameServer.Utility;
using System;
using System.Buffers;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace GameServer.UnityClient.Components.Messaging
{
    public class LengthPrefixedTypedMessageHandler
    {
        private readonly MessagePackMessageSerializer msgSerializer;

        private Dictionary<int, Tuple<Type, Delegate>> registeredMessages = new Dictionary<int, Tuple<Type, Delegate>>();

        const int LENGTH_PREFIX_WIDTH = 2;
        const int MESSAGE_ID_PREFIX_WIDTH = 2;
        const int CLIENT_ID_PREFIX_WIDTH = 22;
        const int STREAM_FRAME_HEADER_WIDTH = LENGTH_PREFIX_WIDTH + MESSAGE_ID_PREFIX_WIDTH;
        const int DATAGRAM_FRAME_HEADER_WIDTH = LENGTH_PREFIX_WIDTH + MESSAGE_ID_PREFIX_WIDTH + CLIENT_ID_PREFIX_WIDTH;
        const int MAX_MESSAGE_LENGTH = 2048;

        private byte[] clientId;

        public LengthPrefixedTypedMessageHandler(MessagePackMessageSerializer msgSerializer)
        {
            this.msgSerializer = msgSerializer;
        }

        public string ClientId
        {
            get
            {
                return Encoding.UTF8.GetString(clientId);
            }

            set
            {
                clientId = Encoding.UTF8.GetBytes(value);
            }
        }

        public bool TryParse(ReadOnlySequence<byte> buffer, out Message message, out SequencePosition advancedTo)
        {
            long bytesReceived = buffer.Length;

            if (bytesReceived < STREAM_FRAME_HEADER_WIDTH)
            {
                message = null;
                advancedTo = buffer.Start;
                return false;
            }

            short messageLength;
            short messageId;

            if (buffer.First.Length >= STREAM_FRAME_HEADER_WIDTH)
                messageLength = ParseFrameHeader(buffer.First.Span, out messageId);
            else
            {
                Span<byte> local = stackalloc byte[STREAM_FRAME_HEADER_WIDTH];
                buffer.Slice(0, STREAM_FRAME_HEADER_WIDTH).CopyTo(local);
                messageLength = ParseFrameHeader(local, out messageId);
            }

            if (bytesReceived - STREAM_FRAME_HEADER_WIDTH >= messageLength)
            {
                byte[] content;

                if (buffer.First.Length >= messageLength - STREAM_FRAME_HEADER_WIDTH)
                    content = buffer.First.Slice(STREAM_FRAME_HEADER_WIDTH, messageLength).ToArray();
                else
                {
                    Span<byte> local = stackalloc byte[messageLength];
                    buffer.Slice(STREAM_FRAME_HEADER_WIDTH, messageLength).CopyTo(local);
                    content = local.ToArray();
                }

                message = (Message)registeredMessages[messageId].Item2.DynamicInvoke(new object[] { content });

                advancedTo = buffer.GetPosition(STREAM_FRAME_HEADER_WIDTH + messageLength);

                return true;
            }
            else
            {
                message = null;
                advancedTo = buffer.Start;
                return false;
            }
        }

        public void RegisterMessagesInAssembly(Assembly assy)
        {
            foreach (Type type in assy.GetTypes())
                RegisterAttributedType(type);

        }

        public void RegisterAttributedTypes(Type[] types)
        {
            foreach (Type type in types)
                RegisterAttributedType(type);
        }

        public void RegisterAttributedType(Type type)
        {
            object[] attribs = type.GetCustomAttributes(typeof(MessageObjectAttribute), true);

            if (attribs.Length == 1)
            {
                Delegate deserializer = GetDeserializer(type);
                registeredMessages.Add(((MessageObjectAttribute)attribs[0]).Id, new Tuple<Type, Delegate>(type, GetDeserializer(type)));
            }
        }

        public void RegisterType(int messageId, Type type)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a delegate of a method that serializes a given type
        /// </summary>
        /// <param name="genericType">The <see cref="Type"/> of the object that the method serializes</param>
        /// <returns>The <see cref="Delegate"/> of the method that serializes the given <see cref="Type"/></returns>
        private Delegate GetDeserializer(Type genericType)
        {
            var del = typeof(Func<,>).MakeGenericType(typeof(byte[]), genericType);
            return msgSerializer.GetType().GetMethod("Deserialize").MakeGenericMethod(genericType).CreateDelegate(del, msgSerializer);
        }

        public int Prepare(Message message, Span<byte> buffer, bool includeId = false)
        {
            byte[] content = msgSerializer.Serialize(message);

            content.CopyTo(buffer.Slice(STREAM_FRAME_HEADER_WIDTH));

            BinaryPrimitives.WriteInt16BigEndian(buffer, (short)content.Length);
            BinaryPrimitives.WriteInt16BigEndian(buffer.Slice(LENGTH_PREFIX_WIDTH), (short)registeredMessages.First(x => x.Value.Item1 == message.GetType()).Key);

            if(includeId)
            {
                clientId.AsSpan().CopyTo(buffer.Slice(LENGTH_PREFIX_WIDTH + MESSAGE_ID_PREFIX_WIDTH));
                return DATAGRAM_FRAME_HEADER_WIDTH + content.Length;
            }

            return STREAM_FRAME_HEADER_WIDTH + content.Length;
        }

        private short ParseFrameHeader(ReadOnlySpan<byte> bytes, out short id)
        {

            short length = BinaryPrimitives.ReadInt16BigEndian(bytes);

            id = BinaryPrimitives.ReadInt16BigEndian(bytes.Slice(2));

            return length;
        }
    }
}
