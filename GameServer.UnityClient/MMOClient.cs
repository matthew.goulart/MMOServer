﻿using GameServer.Components.Messaging.Serializer;
using GameServer.Components.Messaging.Service;
using GameServer.Messages;
using GameServer.UnityClient.Components.Messaging;
using GameServer.UnityClient.Entities;
using Pipelines.Sockets.Unofficial;
using System;
using System.IO.Pipelines;
using System.Net.Sockets;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace GameServer.UnityClient
{
    /// <summary>
    /// A lightweight client that implements Pipes (System.IO.Pipelines) to
    /// make server communication as fast as possible.
    /// </summary>
    public partial class MmoClient : MonoBehaviour
    {
        private const int MAX_MESSAGE_LENGTH = 2048;

        public LocalPlayer PlayerEntity { get; set; }

        private TcpClient tcpClient;
        IDuplexPipe tcpClientPipe;

        private UdpClient UdpClient;
        private LengthPrefixedTypedMessageHandler msgHandler;

        public string Address;
        public int Port;

        public async virtual void Start()
        {
            tcpClient = new TcpClient(Address, Port);            
            UdpClient = new UdpClient();

            //Message Service
            MessageService = MessageService.Instance;//Get the singleton instance
            MessageService.SubscribeAll(this);//Subscribe any message handlers in this class

            //Message Handler
            var serializer = new MessagePackMessageSerializer();
            msgHandler = new LengthPrefixedTypedMessageHandler(serializer);            
            msgHandler.RegisterMessagesInAssembly(Assembly.GetAssembly(typeof(Message)));

            if(!tcpClient.Connected)
            {
                throw new SocketException((int)SocketError.HostUnreachable);
            }

            tcpClientPipe = StreamConnection.GetDuplex(tcpClient.GetStream());

            await DoReceiveAsync();
        }

        public MessageService MessageService { get; private set; }

        /// <summary>
        /// Subscribes all message handlers on the giuven object
        /// </summary>
        /// <param name="target">Object containing the message handlers to subscribe</param>
        public void SubscribeAllMessages(object target)
        {
            MessageService.SubscribeAll(target);
        }

        void OnApplicationQuit()
        {
            tcpClientPipe.Input.Complete(new ApplicationException("Application is closing"));
            tcpClientPipe.Output.Complete(new ApplicationException("Application is closing"));
            tcpClient.Close();
            UdpClient.Close();

            Debug.Log("Application ending after " + Time.time + " seconds");
        }
    }
}
