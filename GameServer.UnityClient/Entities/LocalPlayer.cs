﻿using GameServer.Entities;
using GameServer.UnityClient.Utility;
using System;
using UnityEngine;

namespace GameServer.UnityClient.Entities
{
    public class LocalPlayer : MonoBehaviour
    {        
        private Vector3 lastPosition;
        private float lastRotation;

        private string title;
        private string name;
        private int level;
        private HumanoidPosture posture;
        private PlayerPresence presence;

        public bool StateChanged { get; set; } = false;
        public bool PositionChanged { get; set; } = false;
        public DateTime LastStateChange { get; private set; }
        public DateTime LastStateRead { get; private set; }

        #region Position
        public Vector3 Position
        {
            get { return gameObject.transform.position; }
            set
            {
                gameObject.transform.position = value;
            }
        }

        public float Rotation
        {
            get { return gameObject.transform.rotation.eulerAngles.y; }
            set
            {
                gameObject.transform.rotation = Quaternion.AngleAxis(value, Vector3.up);
            }
        }
        #endregion

        #region State
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                stateUpdate.Title = value;
                RegisterStateUpdate();
            }
        }

        public string Name => name;
        public int Level => level;

        public HumanoidPosture Posture
        {
            get { return posture; }
            set
            {
                posture = value;
                stateUpdate.Posture = value;
                RegisterStateUpdate();
            }
        }

        public PlayerPresence Presence
        {
            get { return presence; }
            set
            {
                presence = value;
                stateUpdate.Presence = value;
                RegisterStateUpdate();
            }
        }
        #endregion

        private PlayerState stateUpdate = new PlayerState();

        void Start()
        {

        }

        void Update()
        {
            UpdatePosition();
        }

        private void UpdatePosition()
        {
            if (gameObject.transform.position != lastPosition)
            {
                lastPosition = gameObject.transform.position;
                PositionChanged = true;
            }                

            if (gameObject.transform.rotation.eulerAngles.y != lastRotation)
            {
                lastRotation = gameObject.transform.rotation.eulerAngles.y;
                PositionChanged = true;
            }
        }

        public PlayerState GetStateUpdate()
        {
            PlayerState newState;

            lock (stateUpdate)
            {
                newState = (PlayerState)stateUpdate.Clone();

                StateChanged = false;

                stateUpdate = new PlayerState();
            }
            return newState;
        }

        public void ApplyState(PlayerState state)
        {
            if (state.Position.HasValue)
                Position = state.Position.Value.ToVector3();

            if (state.Rotation.HasValue)
                Rotation = state.Rotation.Value;

            title = state.Title ?? title;
            name = state.Name ?? name;
            level = state.Level ?? level;
            posture = state.Posture ?? posture;
            presence = state.Presence ?? presence;
        }

        protected void RegisterStateUpdate()
        {
            StateChanged = true;
            LastStateChange = DateTime.Now;
        }
    }
}
