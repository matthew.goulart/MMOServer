﻿using GameServer.Messages;
using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.UnityClient
{
    public partial class MmoClient
    {
        /// <summary>
        /// Queues a message to be sent to the server in the pipe.
        /// </summary>
        /// <param name="message">The message to be sent</param>
        /// <returns>A task that will complete when the pipe flush succeeds</returns>
        public Task Send(Message message)
        {
            PipeWriter writer = tcpClientPipe.Output;

            Span<byte> buffer = tcpClientPipe.Output.GetSpan(MAX_MESSAGE_LENGTH);

            int bytesWritten = msgHandler.Prepare(message, buffer);

            tcpClientPipe.Output.Advance(bytesWritten);

            return FlushAsync(writer);
        }

        /// <summary>
        /// Flushing asynchronously in a separate method allows the <see cref="Send(Message)"/>
        /// method to use <see cref="Span{T}"/> instead of <see cref="Memory{T}"/>.
        /// </summary>
        /// <param name="writer">The writer to flush</param>
        private static async Task FlushAsync(PipeWriter writer)
        {
            // apply back-pressure etc
            var flush = await writer.FlushAsync();
        }

        /// <summary>
        /// Sends a message to the server and expects a reply.
        /// </summary>
        /// <typeparam name="TReply">The <see cref="Type"/> expected in reply</typeparam>
        /// <param name="message">The <see cref="Message"/> to send</param>
        /// <returns>An awaitable task that will resolve to the reply if succesful.</returns>
        public async Task<TReply> SendWithReplyAsync<TReply>(Message message) where TReply : Message
        {
            var task = MessageService.RegisterReplyHandler(message);

            await Send(message).ConfigureAwait(false);

            return (TReply)await task;
        }
    }
}
