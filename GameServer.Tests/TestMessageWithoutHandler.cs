﻿using GameServer.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Tests
{
    class TestMessageWithoutHandler : Message
    {
        public int TestInt { get; set; }
        public string TestString { get; set; }
    }
}
