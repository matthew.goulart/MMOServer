﻿using GameServer.Components.Messaging.Handler;
using GameServer.Components.Messaging.Serializer;
using GameServer.Messages;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Buffers;
using System.Linq;

namespace GameServer.Tests
{
    [TestClass]
    public class MessageHandlerTests
    {
        private MessagePackMessageSerializer msgSerializer;
        private LengthPrefixedTypedMessageHandler msgHandler;


        [TestInitialize]
        public void TestInitialize()
        {
            Mock<ILogger> mockLog = new Mock<ILogger>();
            msgSerializer = new MessagePackMessageSerializer();
            msgHandler = new LengthPrefixedTypedMessageHandler(mockLog.Object, msgSerializer);

            msgHandler.RegisterAttributedType(typeof(TestMessage));
        }

        [TestMethod]
        public void MessageSerialization()
        {
            TestMessage testMessage = new TestMessage() { TestInt = 101, TestString = "Testing" };

            var serializedTestMessage = GetSerializedTestMessage();
            Span<byte> serializedMessage = new byte[256];
            msgHandler.Prepare(testMessage, serializedMessage, out int bytesWritten);

            Assert.AreEqual(serializedTestMessage.Length, bytesWritten);

            Assert.AreEqual(serializedTestMessage, serializedMessage.ToArray());
        }

        [TestMethod]
        public void MessageDeserialization()
        {
            TestMessage testMessage = new TestMessage() { TestInt = 101, TestString = "Testing" };
            Message deserializedTestMessage = null;

            ReadOnlySequence<byte> serializedTestMessage = new ReadOnlySequence<byte>(GetSerializedTestMessage());

            var result = msgHandler.TryParseStream(serializedTestMessage, out deserializedTestMessage, out SequencePosition advancedTo);

            Assert.IsTrue(result);
            Assert.AreEqual(testMessage.TestInt, ((TestMessage)deserializedTestMessage).TestInt);
            Assert.AreEqual(testMessage.TestString, ((TestMessage)deserializedTestMessage).TestString);
        }

        private byte[] GetSerializedTestMessage()
        {
            TestMessage testMessage = new TestMessage() { TestInt = 101, TestString = "Testing" };

            var data = msgSerializer.Serialize(testMessage);
            var length = data.Length + 2;
            var id = ((MessageObjectAttribute)typeof(TestMessage).GetCustomAttributes(typeof(MessageObjectAttribute), true).FirstOrDefault()).Id;

            byte[] serializedTestMessage = new byte[data.Length + 2 + 2];

            Buffer.BlockCopy(BitConverter.GetBytes((short)length), 0, serializedTestMessage, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)id), 0, serializedTestMessage, 2, 2);
            Buffer.BlockCopy(data, 0, serializedTestMessage, 2 + 2, data.Length);

            return serializedTestMessage;
        }
    }
}
