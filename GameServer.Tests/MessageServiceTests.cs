﻿using GameServer.Components.Messaging.Service;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GameServer.Tests
{
    [TestClass]
    public class MessageServiceTests
    {
        MessageService msgService;

        TestMessage handledMessage;

        [TestInitialize]
        public void TestInitialize()
        {
            Mock<ILogger> mockLog = new Mock<ILogger>();

            msgService = new MessageService(mockLog.Object);

            msgService.SubscribeAll(this);
        }

        /// <summary>
        /// Tests that a handler is correctly called for a subscribed message
        /// </summary>
        [TestMethod]
        public void HandlerInvocation()
        {
            handledMessage = null;

            TestMessage testMessage = new TestMessage() { TestInt = 1, TestString = "Test" };

            msgService.Send(testMessage);

            Assert.AreEqual(testMessage, handledMessage);
        }

        /// <summary>
        /// Tests that a handly is correctly called for a subscribed message and that the task returns the expected object
        /// </summary>
        [TestMethod]
        public void AsyncHandlerInvocationWithReply()
        {
            TestMessage testMessage = new TestMessage() { TestInt = 1, TestString = "Test" };

            var reply = msgService.SendWithReplyAsync<TestMessage, TestMessage>(testMessage).Result;

            Assert.AreEqual(testMessage, reply);
        }

        /// <summary>
        /// Tests that sending a message with no handlers generates a <see cref="NoHandlersException"/>
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NoHandlersException))]
        public void NoHandlerForMessage()
        {
            TestMessageWithoutHandler testMessage = new TestMessageWithoutHandler() { TestInt = 1, TestString = "Test" };

            msgService.Send(testMessage);
        }

        //Test handlers
        [Handles(typeof(TestMessage))]
        public void HandleTestMessage(object sender, TestMessage message)
        {
            handledMessage = message;
        }

        [Handles(typeof(TestMessage))]
        public TestMessage HandleTestMessageWithReply(object sender, TestMessage message)
        {
            return message;
        }
    }
}
