﻿using GameServer.Messages;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Tests
{
    [MessageObject(1)]
    [MessagePackObject]
    public class TestMessage : Message
    {
        [Key(1)]
        public int TestInt { get; set; }
        [Key(2)]
        public string TestString { get; set; }
    }
}
