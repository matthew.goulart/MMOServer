﻿using System;

namespace GameServer.Messages
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class MessageObjectAttribute : Attribute
    {
        public int Id { get; set; }

        public MessageObjectAttribute(int Id)
        {
            this.Id = Id;
        }
    }
}
