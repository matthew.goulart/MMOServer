﻿using GameServer.Entities;
using MathNet.Spatial.Euclidean;
using MessagePack;
using System;
using System.Collections.Generic;

namespace GameServer.Messages
{
    #region Server Stats
    [MessageObject(0)]
    [MessagePackObject]
    public class StatisticsRequest : Message { }

    [MessageObject(10)]
    [MessagePackObject]
    public class StatisticsReply : Message
    {
        [Key(1)]
        public int TotalBytesReceived { get; set; }

        [Key(2)]
        public int TotalBytesSent { get; set; }

        [Key(3)]
        public int CurrentConnectedCliets { get; set; }

        [Key(4)]
        public TimeSpan Uptime { get; set; }

        [Key(5)]
        public int AverageLoad { get; set; }
    }
    #endregion

    #region Internal
    [MessageObject(20)]
    public class PlayerDisconnected : Message
    {
        [Key(1)]
        public Guid Id { get; set; }
    }
    #endregion

    #region Login
    [MessageObject(30)]
    [MessagePackObject]
    public class LoginRequest : Message
    {
        [Key(1)]
        public string Username { get; set; }
        [Key(2)]
        public string Password { get; set; }
    }

    [MessageObject(40)]
    [MessagePackObject]
    public class LoginResponse : Message
    {
        [Key(1)]
        public bool Result { get; set; }
        [Key(2)]
        public string ConnectTo { get; set; }
        [Key(3)]
        public string Error { get; set; }
    }
    #endregion

    #region Spawn
    [MessageObject(50)]
    [MessagePackObject]
    public class PlayerSpawnRequest : Message
    {
    }

    [MessageObject(60)]
    [MessagePackObject]
    public class PlayerSpawnResponse : Message
    {
        [Key(1)]
        public Guid SpawnZone { get; set; }

        [Key(2)]
        public PlayerState InitialState { get; set; }

        [Key(3)]
        public short TickRateMs { get; set; } 
    }

    [MessageObject(70)]
    [MessagePackObject]
    public class PlayerReady : Message { }
    #endregion

    #region Entity State synchronization
    /// <summary>
    /// TO SERVER
    /// A state update from a player
    /// </summary>
    [MessageObject(80)]
    [MessagePackObject]
    public class PlayerStateUpdate : Message
    {
        [Key(1)]
        public PlayerState State { get; set; }
    }

    /// <summary>
    /// TO SERVER
    /// A position update from a player
    /// </summary>
    [MessageObject(90)]
    [MessagePackObject]
    public class PlayerPositionUpdate : Message
    {
        [Key(1)]
        public Point3D? Position { get; set; }

        [Key(2)]
        public float? Rotation { get; set; }
    }

    /// <summary>
    /// TO PLAYER
    /// Collection of new (previously not visible) entities to spawn
    /// </summary>
    [MessageObject(100)]
    [MessagePackObject]
    public class SpawnEnities : Message
    {
        [Key(1)]
        public List<EntityState> NewEntities { get; set; } = new List<EntityState>();
    }

    [MessageObject(110)]
    [MessagePackObject]
    public class DestroyEntities : Message
    {
        [Key(1)]
        public List<Guid> Entities { get; set; } = new List<Guid>();
    }

    /// <summary>
    /// TO PLAYER
    /// Collection of updated states of visible entities
    /// </summary>
    [MessageObject(120)]
    [MessagePackObject]
    public class EntityStateUpdates : Message
    {
        [Key(1)]
        public List<EntityState> EntityStates { get; set; } = new List<EntityState>();
    }

    /// <summary>
    /// TO PLAYER
    /// Collection of updated positions of visible entities
    /// </summary>
    [MessageObject(130)]
    [MessagePackObject]
    public class EntityPositionUpdates : Message
    {
        [Key(1)]
        public Dictionary<Guid, EntityPosition> EntityPositions { get; set; } = new Dictionary<Guid, EntityPosition>();
    }
    #endregion

    #region Dialogue
    [MessageObject(140)]
    [MessagePackObject]
    public class DialogueRequest : Message
    {
        public Guid Target { get; set; }
    }

    [MessageObject(150)]
    [MessagePackObject]    
    public class DialogueReply : Message
    {
        public string Dialogue { get; set; }
    }
    #endregion
}
