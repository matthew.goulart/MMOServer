﻿using GameServer.Utility;
using MessagePack;
using System;

namespace GameServer.Messages
{
    [MessagePackObject]
    public abstract class Message
    {
        [Key(0)]
        public ShortGuid? ReplyToken { get; set; }

        public TReply GetReply<TReply>() where TReply : Message, new()
        {
            if (!ReplyToken.HasValue)
                throw new InvalidOperationException("No reply token was set for this message. A reply cannot be generated.");

            var reply = new TReply() { ReplyToken = ReplyToken };

            return reply;
        }
    }
}
