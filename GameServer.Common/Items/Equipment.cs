﻿namespace GameServer.Items
{
    public class Equipment : Item
    {
        public EquipmentSlot Slot { get; set; }
    }
}
