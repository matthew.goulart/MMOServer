﻿namespace GameServer.Items
{
    public enum ItemRarity
    {
        Common,
        Uncommon,
        Rare,
        Legendary,
        Godly
    }
}
