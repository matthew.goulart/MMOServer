﻿namespace GameServer.Items
{
    public enum EquipmentSlot
    {
        HeldLeftHand,
        HeldRightHand,
        LeftHand,
        RightHand,
        LeftArm,
        RightArm,
        LeftShoulder,
        RightShoulder,
        LeftFoot,
        RightFoot,
        LeftLeg,
        RightLeg,
        Waist,
        UpperBody,
        Head,
        Face
    }
}
