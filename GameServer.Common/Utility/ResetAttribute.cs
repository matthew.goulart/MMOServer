﻿using System;

namespace GameServer.Utility
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    class ResetAttribute : Attribute
    {
        public DefaultValueType To { get; set; }
    }

    public enum DefaultValueType
    {
        Null,
        Default,
        Clear
    }
}
 