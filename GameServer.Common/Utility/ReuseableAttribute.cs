﻿using System;

namespace GameServer.Utility
{
    [AttributeUsage(AttributeTargets.Class)]
    class ReuseableAttribute : Attribute
    {
    }
}
