﻿namespace GameServer.Utility
{
    public interface IReuseable
    {
        void Reset();
    }
}
