﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;

namespace GameServer.Utility
{
    public static class ObjectResetter
    {
        private static Dictionary<Type, List<Tuple<Delegate, Object>>> propertyCache = new Dictionary<Type, List<Tuple<Delegate, Object>>>();

        static ObjectResetter()
        {
            foreach (Type type in Assembly.GetCallingAssembly().GetTypes().Where(t => t.GetCustomAttribute<ReuseableAttribute>() != null))
            {

                List<Tuple<Delegate, Object>> setters = new List<Tuple<Delegate, object>>();

                foreach(PropertyInfo prop in type.GetRuntimeProperties())
                {
                    var attr = prop.GetCustomAttribute<ResetAttribute>();

                    if (attr == null)
                        continue;
                    
                    switch(attr.To)
                    {
                        case DefaultValueType.Null:
                            setters.Add(new Tuple<Delegate, Object>(prop.GetSetMethod().CreateDelegate(typeof(Action<>)), null));
                            break;
                        case DefaultValueType.Clear:
                            setters.Add(prop.GetSetMethod().)

                    }
                        

                    setters.Add(prop.GetSetMethod().CreateDelegate(typeof(Action<>)));
                }

                propertyCache.Add(type, setters);
            }
        }

        public static T Reset<T>(T target)
        {

            if (!propertyCache.TryGetValue(typeof(T), out List<Delegate> setters))
            {
                setters = MakeSetterDelegatesForType(typeof(T));
            }

            foreach (Delegate setter in setters)
            {
                setter.DynamicInvoke(new object[] { null });
            }

            return target;
        }

        private static List<Delegate> MakeSetterDelegatesForType(Type type)
        {
            List<Delegate> setters = new List<Delegate>();

            var props = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic);

            foreach(PropertyInfo propInfo in props)
            {
                Type delegateType = typeof(Action<>);
                
                Delegate del = propInfo.GetSetMethod().CreateDelegate(delegateType);

                setters.Add(del);
            }

            propertyCache.Add(type, setters);

            return setters;
        }
    }
}
