﻿using MathNet.Spatial.Euclidean;
using MessagePack;
using MessagePack.Formatters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Components.Messaging.Serializer
{
    public class Point3DFormatter : IMessagePackFormatter<Point3D>
    {
        public static readonly Point3DFormatter Instance = new Point3DFormatter();

        Point3DFormatter()
        {
        }

        public Point3D Deserialize(byte[] bytes, int offset, IFormatterResolver formatterResolver, out int readSize)
        {
            var startOffset = offset;
            var count = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
            offset += readSize;

            if (count != 3) throw new InvalidOperationException("Invalid Point3D format.");

            var X = MessagePackBinary.ReadDouble(bytes, offset, out readSize);
            offset += readSize;

            var Y = MessagePackBinary.ReadDouble(bytes, offset, out readSize);
            offset += readSize;

            var Z = MessagePackBinary.ReadDouble(bytes, offset, out readSize);
            offset += readSize;

            readSize = offset - startOffset;
            return new Point3D(X, Y, Z);
        }

        public int Serialize(ref byte[] bytes, int offset, Point3D value, IFormatterResolver formatterResolver)
        {
            var startOffset = offset;

            offset += MessagePackBinary.WriteArrayHeader(ref bytes, offset, 3);
            offset += MessagePackBinary.WriteDouble(ref bytes, offset, value.X);
            offset += MessagePackBinary.WriteDouble(ref bytes, offset, value.Y);
            offset += MessagePackBinary.WriteDouble(ref bytes, offset, value.Z);

            return offset - startOffset;
        }
    }

    public class NullablePoint3DFormatter : IMessagePackFormatter<Point3D?>
    {
        public static readonly NullablePoint3DFormatter Instance = new NullablePoint3DFormatter();

        NullablePoint3DFormatter()
        {
        }

        public Point3D? Deserialize(byte[] bytes, int offset, IFormatterResolver formatterResolver, out int readSize)
        {
            var startOffset = offset;
            if (MessagePackBinary.IsNil(bytes, offset))
            {
                readSize = 1;
                return null;
            }
                

            var count = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
            offset += readSize;

            if (count != 3) throw new InvalidOperationException("Invalid Point3D format.");

            var X = MessagePackBinary.ReadDouble(bytes, offset, out readSize);
            offset += readSize;

            var Y = MessagePackBinary.ReadDouble(bytes, offset, out readSize);
            offset += readSize;

            var Z = MessagePackBinary.ReadDouble(bytes, offset, out readSize);
            offset += readSize;

            readSize = offset - startOffset;
            return new Point3D(X, Y, Z);
        }

        public int Serialize(ref byte[] bytes, int offset, Point3D? value, IFormatterResolver formatterResolver)
        {
            if (value.HasValue)
            {
                var startOffset = offset;

                offset += MessagePackBinary.WriteArrayHeader(ref bytes, offset, 3);
                offset += MessagePackBinary.WriteDouble(ref bytes, offset, value.Value.X);
                offset += MessagePackBinary.WriteDouble(ref bytes, offset, value.Value.Y);
                offset += MessagePackBinary.WriteDouble(ref bytes, offset, value.Value.Z);

                return offset - startOffset;
            }
            else
                return MessagePackBinary.WriteNil(ref bytes, offset);
            
        }
    }
}
