﻿using GameServer.Utility;
using MessagePack;
using MessagePack.Formatters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Components.Messaging.Serializer
{
    class GeneralResolver : IFormatterResolver
    {
        public static readonly IFormatterResolver Instance = new GeneralResolver();

        private Dictionary<Type, object> formatters = new Dictionary<Type, object>()
        {
            {typeof(ShortGuid), ShortGuidFormatter.Instance },
            {typeof(ShortGuid?), NullableShortGuidFormatter.Instance }
        };

        public IMessagePackFormatter<T> GetFormatter<T>()
        {
            object formatter;

            if (formatters.TryGetValue(typeof(T), out formatter))
                return (IMessagePackFormatter<T>)formatter;
            else
                return null;
        }
    }
}
