﻿using MathNet.Spatial.Euclidean;
using MessagePack;
using MessagePack.Formatters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Components.Messaging.Serializer
{
    class MathNetResolver : IFormatterResolver
    {
        public static readonly IFormatterResolver Instance = new MathNetResolver();

        private Dictionary<Type, object> formatters = new Dictionary<Type, object>()
        {
            {typeof(Point3D), Point3DFormatter.Instance },
            {typeof(Point3D?), NullablePoint3DFormatter.Instance }
        };

        public IMessagePackFormatter<T> GetFormatter<T>()
        {
            object formatter;

            if (formatters.TryGetValue(typeof(T), out formatter))
                return (IMessagePackFormatter < T >)formatter;
            else
                return null;
        }
    }
}
