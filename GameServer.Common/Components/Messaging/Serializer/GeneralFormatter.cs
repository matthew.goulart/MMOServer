﻿using GameServer.Utility;
using MessagePack;
using MessagePack.Formatters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Components.Messaging.Serializer
{
    class ShortGuidFormatter : IMessagePackFormatter<ShortGuid>
    {
        public static readonly ShortGuidFormatter Instance = new ShortGuidFormatter();

        public ShortGuid Deserialize(byte[] bytes, int offset, IFormatterResolver formatterResolver, out int readSize)
        {
            var startOffset = offset;

            var chars = MessagePackBinary.ReadString(bytes, offset, out readSize);

            return new ShortGuid(chars);
        }

        public int Serialize(ref byte[] bytes, int offset, ShortGuid value, IFormatterResolver formatterResolver)
        {
            var startOffset = offset;

            offset += MessagePackBinary.WriteString(ref bytes, offset, value.Value);

            return offset - startOffset;
        }
    }

    class NullableShortGuidFormatter : IMessagePackFormatter<ShortGuid?>
    {
        public static readonly NullableShortGuidFormatter Instance = new NullableShortGuidFormatter();

        public ShortGuid? Deserialize(byte[] bytes, int offset, IFormatterResolver formatterResolver, out int readSize)
        {
            var startOffset = offset;

            if (MessagePackBinary.IsNil(bytes, offset))
            {
                readSize = 1;
                return null;
            }
                

            var chars = MessagePackBinary.ReadString(bytes, offset, out readSize);

            return new ShortGuid?(chars);
        }

        public int Serialize(ref byte[] bytes, int offset, ShortGuid? value, IFormatterResolver formatterResolver)
        {
            var startOffset = offset;

            if(value.HasValue)
            {
                offset += MessagePackBinary.WriteString(ref bytes, offset, value.Value);
            }
            else
            {
                offset += MessagePackBinary.WriteNil(ref bytes, offset);
            }
            

            return offset - startOffset;
        }
    }
}
