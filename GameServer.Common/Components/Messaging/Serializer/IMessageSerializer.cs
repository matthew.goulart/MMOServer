﻿using GameServer.Messages;

namespace GameServer.Components.Messaging.Serializer
{
    public interface IMessageSerializer
    {
        byte[] Serialize(object message);
        TMessage Deserialize<TMessage>(byte[] data) where TMessage : Message;
        dynamic DeserializeTypeless(byte[] data);
    }
}
