﻿using GameServer.Messages;
using MessagePack;
using MessagePack.Resolvers;
using System;

namespace GameServer.Components.Messaging.Serializer
{
    public class MessagePackMessageSerializer : IMessageSerializer
    {
        public MessagePackMessageSerializer()
        {
            CompositeResolver.RegisterAndSetAsDefault(
                new[] {
                    MathNetResolver.Instance,
                    GeneralResolver.Instance,
                    StandardResolver.Instance,
                    //ContractlessStandardResolver.Instance
                });
        }

        public TMessage Deserialize<TMessage>(byte[] data) where TMessage : Message
        {
#if DEBUG
            return MessagePackSerializer.Deserialize<TMessage>(data);
#else
            return LZ4MessagePackSerializer.Deserialize<TMessage>(data);
#endif
        }

        public dynamic DeserializeTypeless(byte[] data)
        {
            throw new NotImplementedException();
        }

        public byte[] Serialize(object message)
        {
#if DEBUG
            return MessagePackSerializer.Serialize(message);
#else
            return LZ4MessagePackSerializer.Serialize(message);
#endif
        }
    }
}
