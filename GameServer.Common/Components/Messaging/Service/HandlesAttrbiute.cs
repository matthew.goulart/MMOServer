﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Components.Messaging.Service
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HandlesAttribute : Attribute
    {
        public Type MessageType { get; private set; }

        public HandlesAttribute(Type messageType)
        {
            MessageType = messageType;
        }
    };
}
