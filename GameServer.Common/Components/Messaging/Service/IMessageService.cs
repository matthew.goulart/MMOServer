﻿using GameServer.Messages;
using GameServer.Utility;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace GameServer.Components.Messaging.Service
{
    public interface IMessageService
    {
        void SubscribeAll(object target);
        void Subscribe(Type msgType, Delegate handler);
        Task<Message> RegisterReplyHandler(Message message, int timeout = 60000);
        void Send<TMessage, TSender>(TMessage message, TSender sender = default) where TMessage : Message where TSender : class;
        Task<TReply> SendWithReplyAsync<TReply, TMessage, TSender>(TMessage message, TSender sender = default, int timeout = 60000) where TReply : Message where TMessage : Message where TSender : class;

        event EventHandler<Message> MessageReceived;
    }
}
