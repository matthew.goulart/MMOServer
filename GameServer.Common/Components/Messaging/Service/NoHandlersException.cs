﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Components.Messaging.Service
{
    public class NoHandlersException : Exception
    {
        public NoHandlersException()
            :base()
        {

        }

        public NoHandlersException(string message)
            :base(message)
        {

        }

        public NoHandlersException(string message, Exception innerException)
            :base(message, innerException)
        {

        }
    }
}
