﻿using GameServer.Messages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace GameServer.Components.Messaging.Service
{
    //TODO Implement the queue
    //TODO Implement per-instance handle-less queue
    public class MessageService : IMessageService
    {
        public const int MAX_REPLY_WAIT_MS = 60000;

        ILogger log;

        private Dictionary<Type, List<Delegate>> messageHandlers = new Dictionary<Type, List<Delegate>>();
        private Dictionary<Guid, TaskCompletionSource<Message>> replyTasks = new Dictionary<Guid, TaskCompletionSource<Message>>();

        private static MessageService instance;

        public MessageService(ILogger log = null)
        {
            this.log = log;
        }

        public static MessageService Instance
        {
            get
            {
                if (instance == null)
                    instance = new MessageService();

                return instance;
            }
        }

        private void HandleMessage<TMessage, TSender>(TMessage message, TSender sender = null) where TMessage : Message where TSender : class
        {
            //Check if the message is in reply to a previously sent one.
            //If it is, we can complete the reply task with the result
            if (message.ReplyToken.HasValue && 
                replyTasks.TryGetValue(message.ReplyToken.Value, out TaskCompletionSource<Message> tcs) && 
                !tcs.Task.IsCanceled)
            {

                tcs.SetResult(message);
                return;
            }

            //The message is not a reply, so we can invoke the associated handlers as usual
            var messageType = message.GetType();

            if (messageHandlers.TryGetValue(messageType, out List<Delegate> handlers))
            {

                foreach (var handler in handlers)
                {
                    try
                    {
                        //If the message type is specific enough, we should be able to .Ivoke(),
                        //otherwise we have to use .DynamicInvoke() which is much slower
                        if (handler is Action<TSender, TMessage> h)
                        {
#if (ENV_NOT_THREAD_SAFE)
                            h(sender, message);
#else
                            Task.Factory.StartNew(() => h(sender, message));                            
#endif
                        }
                        else
                        {
#if (ENV_NOT_THREAD_SAFE)
                            handler.DynamicInvoke(sender, message);
#else
                            Task.Factory.StartNew(() => handler.DynamicInvoke(sender, message));                            
#endif
                        }

                    }
                    catch(Exception e)
                    {
                        log?.LogError($"Attempt to handle a message failed. {e.Message}");
                    }
                }
            }
            else
            {
                log?.LogError(string.Format("No handler found for message of type {0}", messageType.FullName));
                throw new NoHandlersException(messageType.FullName);
            }
        }

        /// <summary>
        /// Adds a <see cref="TaskCompletionSource{Message}"/> to the <see cref="replyTasks"/> collection
        /// in order to be able to complete the task if/when a reply arrives with the associated ID
        /// </summary>
        /// <param name="message">The initial message (the one being replied to)</param>
        /// <param name="timeout">Optional: Amount of time (ms) to wait before cancelling the task</param>
        /// <returns>The task that will either return the reply or indicate a timeout</returns>
        public Task<Message> RegisterReplyHandler(Message message, int timeout = MAX_REPLY_WAIT_MS)
        {
            var replyToken = Guid.NewGuid();

            var completionSource = new TaskCompletionSource<Message>();
            var tokenSource = new CancellationTokenSource();

            tokenSource.CancelAfter(timeout);
            //TODO Make sure there is no leakage with the call to Token.Register() 
            tokenSource.Token.Register(() =>
            {
                completionSource.TrySetCanceled();
                if (replyTasks.ContainsKey(replyToken))
                    replyTasks.Remove(replyToken);
            },
                false);

            replyTasks.Add(replyToken, completionSource);

            message.ReplyToken = replyToken;
            return completionSource.Task;
        }

#region IMessageSrvice implementation
        /// <summary>
        /// Queues a message to be sent
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        public void Send<TMessage, TSender>(TMessage message, TSender sender = null) where TMessage : Message where TSender : class => HandleMessage(message, sender);
        public void Send<TMessage>(TMessage message, object sender = null) where TMessage : Message => Send<TMessage, object>(message, sender);

        /// <summary>
        /// Queues a message to be sent internally (between users of the message service) and returns a task that will return the reply message once the work is complete
        /// </summary>
        /// <typeparam name="TReply">The expected <see cref="Type"/> of the reply</typeparam>
        /// <param name="message">The <see cref="Message"/> to send</param>
        /// <param name="sender">The original sender of the message</param>
        /// <param name="timeout">The amont of time (in milliseconds) to wait for a reply</param>
        /// <returns>A task that will return the reply of the appropriate type once it completes</returns>
        public async Task<TReply> SendWithReplyAsync<TReply, TMessage, TSender>(TMessage message, TSender sender = default, int timeout = MAX_REPLY_WAIT_MS) where TReply : Message where TMessage : Message where TSender : class
        {
            var task = RegisterReplyHandler(message, timeout);

            HandleMessage(message, sender);

            return (TReply) await task;
        }

        public async Task<TReply> SendWithReplyAsync<TReply, TMessage>(TMessage message, object sender = null, int timeout = MAX_REPLY_WAIT_MS) where TReply : Message where TMessage : Message
        {
            return await SendWithReplyAsync<TReply, TMessage, object>(message, sender, timeout);
        }
       
        /// <summary>
        /// Adds the given <see cref="Action{object, IMessage}"/> to the list of message handlers for the give <see cref="Type"/>.
        /// </summary>
        /// <param name="msgType">The typ of the message</param>
        /// <param name="handler">The <see cref="Action{object, IMessage}"/> that hadles the message</param>
        public void Subscribe(Type msgType, Delegate handler)
        {
            if (!messageHandlers.TryGetValue(msgType, out List<Delegate> handlers))
            {
                // If there are no handlers attached to this message type, create a new list.
                handlers = new List<Delegate>();
                messageHandlers[msgType] = handlers;
            }

            handlers.Add(handler);
        }

        /// <summary>
        /// Subscribes all methods with the <see cref="HandlesAttribute"/> to the given <see cref="Message"/> <see cref="Type"/>
        /// </summary>
        /// <param name="target">The object to inspect</param>
        public void SubscribeAll(object target)
        {
            var targetType = target.GetType();

            // Get all private and public methods.
            var methods = targetType.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (var method in methods)
            {
                // If this method doesn't have the Handles attribute then ignore it.
                var handlesAttributes = (HandlesAttribute[])method.GetCustomAttributes(typeof(HandlesAttribute), false);
                if (handlesAttributes.Length == 0)
                    continue;

                // The method must have only one argument.
                var parameters = method.GetParameters();
                if (parameters.Length != 2)
                {
                    log?.LogWarning(string.Format("Method {0} has too many arguments", method.Name));
                    continue;
                }

                // That one argument must be derived from Message.
                if (!typeof(Message).IsAssignableFrom(parameters[1].ParameterType))
                {
                    log?.LogWarning(string.Format("Method {0} does not have a Message as it's second argument", method.Name));
                    continue;
                }

                Type genericDelegate;

                genericDelegate = typeof(Action<,>).MakeGenericType(parameters[0].ParameterType, handlesAttributes[0].MessageType);

                var handler = method.CreateDelegate(genericDelegate, target);

                // Success, so register!
                Subscribe(
                    handlesAttributes[0].MessageType,
                    handler);
            }
        }
#endregion

        /// <summary>
        /// Event to be fired when a message is received that targets the given component directly
        /// </summary>
        public event EventHandler<Message> MessageReceived;
    }
}
