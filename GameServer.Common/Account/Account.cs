﻿using GameServer.Entities;
using System;
using System.Collections.Generic;

namespace GameServer.Common.Account
{
    public class Account
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
