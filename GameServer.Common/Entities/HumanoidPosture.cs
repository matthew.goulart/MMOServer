﻿namespace GameServer.Entities
{
    public enum HumanoidPosture
    {
        Standing,
        Crouched,
        Prone
    }
}
