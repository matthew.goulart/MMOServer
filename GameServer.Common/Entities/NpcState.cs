﻿using MessagePack;
using System;

namespace GameServer.Entities
{
    [MessagePackObject]
    public class NpcState : HumanoidState
    {
        [Key(8)]
        bool HasDialogue { get; set; }
        [Key(9)]
        bool HasQuest { get; set; }
    }
}
