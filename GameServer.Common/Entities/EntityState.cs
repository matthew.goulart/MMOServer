﻿using MathNet.Spatial.Euclidean;
using MessagePack;
using System;

namespace GameServer.Entities
{
    [MessagePackObject]
    [Union(0, typeof(PlayerState))]
    [Union(1, typeof(NpcState))]
    [Union(2, typeof(InanimateState))]
    public abstract class EntityState : ICloneable
    {
        [Key(0)]
        public Guid Id { get; set; }

        [Key(1)]
        public Point3D? Position { get; set; }

        [Key(2)]
        public float? Rotation { get; set; }

        public virtual object Clone()
        {
            return MemberwiseClone();
        }
    }
}
