﻿using MathNet.Spatial.Euclidean;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Entities
{
    [MessagePackObject]
    public struct EntityPosition
    {
        [Key(0)]
        public Point3D Position { get; set; }

        [Key(1)]
        public float Rotation { get; set; }
    }
}
