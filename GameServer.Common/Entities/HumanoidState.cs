﻿using MessagePack;
using System;
using System.Collections.Generic;

namespace GameServer.Entities
{
    [MessagePackObject]
    public class HumanoidState : EntityState
    {
        [Key(3)]
        public string Title { get; set; }
        [Key(4)]
        public string Name { get; set; }
        [Key(5)]
        public int? Level { get; set; }
        [Key(6)]
        public HumanoidPosture? Posture { get; set; }
        [Key(7)]
        public Dictionary<HumanoidAppearanceType, int> Appearance { get; set; } = new Dictionary<HumanoidAppearanceType, int>();
    }
}
