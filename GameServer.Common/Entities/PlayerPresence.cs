﻿namespace GameServer.Entities
{
    public enum PlayerPresence
    {
        Present,
        AFK,
        LinkDead
    }
}
