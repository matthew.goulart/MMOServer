﻿using MessagePack;
using System;

namespace GameServer.Entities
{
    [MessagePackObject]
    public class PlayerState : HumanoidState
    {
        [Key(8)]
        public PlayerPresence? Presence { get; set; }
    }
}
